import numpy as np
from scipy.optimize import minimize

def get_xy(r1, r2, d):
    # find lowest point of intersection
    x0, y0, r0, x1, y1, r1 = 0, 0, r1, d, 0, r2
    a, dx, dy, d, h, rx, ry = [0] * 7
    x2, y2 = [0] * 2

    # dx and dy are the vertical and horizontal distances between
    # the circle centers.
    dx = x1 - x0
    dy = y1 - y0

    # Determine the straight-line distance between the centers.
    d = np.sqrt((dy*dy) + (dx*dx))

    # Check for solvability.
    if d > (r0 + r1):
        # no solution. circles do not intersect.
        return False
    if d < abs(r0 - r1): 
        # no solution. one circle is contained in the other
        return False

    # 'point 2' is the point where the line through the circle
    # intersection points crosses the line between the circle
    # centers.  

    # Determine the distance from point 0 to point 2.
    a = ((r0*r0) - (r1*r1) + (d*d)) / (2.0 * d) 

    # Determine the coordinates of point 2.
    x2 = x0 + (dx * a/d)
    y2 = y0 + (dy * a/d)

    # Determine the distance from point 2 to either of the
    #intersection points.
    
    h = np.sqrt((r0*r0) - (a*a))

    # Now determine the offsets of the intersection points from
    # point 2.
    
    rx = -dy * (h/d)
    ry = dx * (h/d)

    # Determine the absolute intersection points.var
    #xi = x2 + rx
    xi_prime = x2 - rx
    #yi = y2 + ry
    yi_prime = y2 - ry

    return xi_prime, yi_prime

def forward(guess, target, d):
    r1, r2 = guess
    cur_xy = get_xy(r1, r2, d)
    if cur_xy == False or r1 < 0 or r2 < 0:
        # invalid lengths
        return 1e6
    t_x, t_y = target
    c_x, c_y = cur_xy
    
    dist = np.sqrt(((t_x - c_x)**2) + ((t_y - c_y)**2))
    return dist

def go(target, d):
    guess = [d/2, d/2]
    res = minimize(forward, guess, args=(target, d, ))
    return res.x

d = 40
r1, r2 = go((13.75, -14), d)
import matplotlib.pyplot as plt
plt.plot([0, 13.75, d], [0, -14, 0])
circle1 = plt.Circle((0, 0), r1, color='r', fill=False)
circle2 = plt.Circle((d, 0), r2, color='r', fill=False)
plt.gca().add_artist(circle1)
plt.gca().add_artist(circle2)
plt.xlim(-r1, d+r2)
plt.ylim(-max([r1, r2]), max([r1,r2]))
plt.show()
