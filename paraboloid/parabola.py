import matplotlib.pyplot as plt
import numpy as np

x_data = np.linspace(0,4,1000)

def f(x):
    return 0.1*(x**2)

fig = plt.figure()
ax = fig.add_subplot(111)

ax.plot(x_data, f(x_data))
ax.set_aspect(aspect=1)

plt.show()
