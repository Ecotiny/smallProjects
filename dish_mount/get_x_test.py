from main_controller import Dish
import numpy as np
import math
import random
import time

if __name__ == "__main__":

    dish_radi = 250
    gantry_height = 600

    # calculate pivot points
    pivots = np.array([[0,0,gantry_height],
                    [0,0,gantry_height],
                    [0,0,gantry_height]])

    tri_length = math.sqrt(3) * dish_radi
    x_offset = tri_length/2
    y_offset = (math.sqrt(3)/6) * tri_length

    # x1,y1
    pivots[0,0] = 0
    pivots[0,1] = dish_radi
    # x2,y2
    pivots[1,0] = x_offset
    pivots[1,1] = y_offset * -1
    # x3,y3
    pivots[2,0] = x_offset * -1 # below 0
    pivots[2,1] = y_offset * -1

    p1 = np.array([pivots[0,0], pivots[0,1], gantry_height])
    p2 = np.array([pivots[1,0], pivots[1,1], gantry_height])
    p3 = np.array([pivots[2,0], pivots[2,1], gantry_height])

    d = Dish(p1, p2, p3, dish_radi*2, 0.01)

    x1 = random.uniform(pivots[2][0], pivots[1][0])
    x2 = random.uniform(pivots[2][0], pivots[1][0])
    x3 = random.uniform(pivots[2][0], pivots[1][0])
    y1 = random.uniform(pivots[2][1], pivots[0][1])
    y2 = random.uniform(pivots[2][1], pivots[0][1])
    y3 = random.uniform(pivots[2][1], pivots[0][1])
    z1 = random.uniform(0, gantry_height)
    z2 = random.uniform(0, gantry_height)
    z3 = random.uniform(0, gantry_height)
    x_start = [x1, y1, z1, x2, y2, z2, x3, y3, z3]

    totals = []
    for _ in range(100):
        start = time.time()
        d.get_x([10,20,30], x_start)
        total = time.time() - start
        totals.append(total)
    print("This took: {:06.5f}s".format(sum(totals)/len(totals)))
