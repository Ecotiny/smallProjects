from printrun.printcore import printcore
import time
import curses
import readchar
p = printcore()
p.connect('/dev/ttyACM0', 250000)
while not p.online: time.sleep(0.1)
p.send_now("G91")

gantry_height = 285

while True:
    key = readchar.readkey()

    # if the 'ESC' key is pressed, Quit
    if key == readchar.key.UP:
        p.send_now("G0 X-1 Y-1")
    elif key == readchar.key.DOWN:
        p.send_now("G0 X1 Y1")
    elif key == readchar.key.LEFT:
        p.send_now("G0 X1 Y-1")
    elif key == readchar.key.RIGHT:
        p.send_now("G0 X-1 Y1")
    elif key == 'q': #q for quit
        p.send_now("G90")
        p.send_now("G0 X-1 Y-1")
        p.send_now("G28")
        break
    elif key == 'h': #h for home
        p.send_now("G90")
        p.send_now("G0 X-1 Y-1")
        p.send_now("G28")
        p.send_now("G91")
    elif key == 'b':
        p.send_now("G90")
        p.send_now("G0 X{} Y{}".format(gantry_height, gantry_height))
        p.send_now("G91")
    # 255 is what the console returns when there is no key press...
    elif key != 255:
        print(key)

p.disconnect()
