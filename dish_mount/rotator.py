import socket
import time

host = "localhost"
port = 4533
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((host, port))
s.listen(0)

while True:
    print("Waiting")
    sock, addr = s.accept()
    
    az, el = [0,0]
    while True:
        data = sock.recv(1024)
        
        if not data:
            break
        print(data)
        if data.startswith('p'):
            sock.sendall("p: {},{}".format(az, el))
        elif data.startswith('P'):
            parsed = data[1:].strip().split(" ")
            az = float(parsed[0])
            el = float(parsed[1])
            print(az,el)
        time.sleep(0.01)
    
    sock.close()
