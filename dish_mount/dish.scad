// dish
$fn=100;
intersection() {
    difference() {
         translate([0,0,100])sphere(r=100);
        translate([0,0,100])sphere(r=99);
    }
    cylinder(r=20, h=30);
}

for (i = [0:2]) {
    rotate(i*120)translate([30,0,-10])union() {
        difference() {
            cube([5,5,40], center=true);
            translate([-2.5,0,-2.5])cube([5, 6, 40], center=true);
        }
        // string
        #translate([0,0,19])rotate([0,-122,0])cylinder(r=0.5, h=12.11);
    }
}