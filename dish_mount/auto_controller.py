import math
import random
import numpy as np
from printrun.printcore import printcore
import time

p = printcore()
p.connect('/dev/ttyACM0', 250000)
while not p.online: time.sleep(0.1)

pivots = [[-125,280],[125,280]]
maxlen = math.sqrt((2*pivots[0][0])**2 + pivots[0][1]**2)
dish_diam = 265

def distance(x1,y1,x2,y2):
    return math.sqrt(abs(x1 - x2)**2 + abs(y1 - y2)**2)

#func takes length of strings, diam of 2d dish, and points at the edge of the dish (to be calculated)
def dish_pos_check(x, l1,l2,d):
    x1,y1,x2,y2 = x
    #distance check of dish points
    dist = distance(x1,y1,x2,y2)
    #distance check to pivot points x val       y val
    dist1_to_p1 = distance(x1,y1,pivots[0][0],pivots[0][1])
    dist2_to_p2 = distance(x2,y2,pivots[1][0],pivots[1][1])
    #sum distance and potential energy
    return (dist1_to_p1 - l1)**2 + (dist2_to_p2 - l2)**2 + (dist - d)**2 + (y1 + y2)/2

    
from scipy.optimize import minimize

def get_x(l1, l2, x_start):
    
    if x_start is None:
        x1 = random.uniform(pivots[0][0], pivots[1][0])
        x2 = random.uniform(x1, pivots[1][0])
        y1 = random.uniform(0, pivots[0][1])
        y2 = random.uniform(0, pivots[1][1])
        x_start = [x1, y1, x2, y2]
    
    d = minimize(dish_pos_check, x_start, args=(l1,l2,dish_diam,))
    return d.x
    #angle = math.atan((y1-y2)/(x1-x2))
    #print(x1,y1,x2,y2, dmin, angle)
    
def get_theta(x):
    x1, y1, x2, y2 = x
    angle = math.atan2((x2-x1), (y2-y1))
    return np.degrees(angle)

def get_length_1(guess_l, prev_l, angle, x_start):
    guess_l1, guess_l2 = guess_l
    prev_l1, prev_l2 = prev_l

    x = get_x(guess_l1, guess_l2, x_start)
            
    theta = get_theta(x)
    
    dl1 = abs(prev_l1 - guess_l1)
    dl2 = abs(prev_l2 - guess_l2)
    
    total_diff = (theta - angle)**4 + (dl1 + dl2)/100#max(dl1, dl2)/2 #devalue difference because we value angle more

    return total_diff

def get_length(prev_l1, prev_l2, angle, x_start):
    
    l_start = [prev_l1, prev_l2]
    prev_l = [prev_l1, prev_l2]
    
    d = minimize(get_length_1, l_start, method = 'Nelder-Mead', args=(prev_l, angle, x_start), options={"maxiter": 300}) # LBthingy bounds = [(0,360),(0,360)],
    print(d)
    print("Diff: {}".format(d.fun))
    return d.x
    
def get_length_old(prev_l1, prev_l2, angle):
    
    #x_start = get_x(prev_l1, prev_l2)
    
    best = 9e99
    
    while True:
        guess_l1 = random.uniform(0,360)
        guess_l2 = random.uniform(0,360)
    
    
        #x = get_x(guess_l1, guess_l2, x_start)
                
        #theta = get_theta(x)
        
        #dl1 = abs(prev_l1 - guess_l1)
        #dl2 = abs(prev_l2 - guess_l2)
        
        #total_diff = (theta - angle)**2 + max(dl1, dl2)
        
        eh = get_length_1([guess_l1,guess_l2],[prev_l1,prev_l2],angle)
        
        if (eh < best):
            best = eh
            print(guess_l1, guess_l2, best)
            
    return total_diff

def test(l1,l2):
    x = get_x(l1,l2)
    print(x)
    print(get_theta(x))
    print(get_length(l1,l2, get_theta(x)+20))
    
dmin = 9e99

desired_angle = 0

l1 = 0.0
l2 = 0.0

def home():
    p.send_now("G0 X-1 Y-1")
    p.send_now("G28")
    p.send_now("G0 X20 Y20")
    l1 = 0.0
    l2 = 0.0
    
def move(l1,l2):
    p.send_now("G0 X{} Y{} F1000".format(l1+20,l2+20))

home()

x_start = [-125,280,125,280]

inny = ""

while inny != 'q':
    inny = input("Desired angle? ")
    if inny != "home":
        try:
            inny = float(inny)
            l1,l2 = get_length(l1,l2,inny,x_start)
            if l1 < 0:
                l1 = 0
            if l2 < 0:
                l2 = 0
            print(l1,l2)
            x_start = get_x(l1,l2, x_start)
            print("End theta: {}".format(get_theta(x_start)))
            move(l1,l2)
        except ValueError:
            print("Wrong")
    else:
        home()

    
