from keras.layers import Input, Dense
from keras.models import Model
import random
import numpy
# training data random values between 0 and 600
training_data = []
t = 0
while t < 500000:
    l1 = float(random.randint(0,600)) / 600.0
    l2 = float(random.randint(0,600)) / 600.0
    l3 = float(random.randint(0,600)) / 600.0
    training_data.append([l1,l2,l3])
    t += 1
    print t

encoding_dim = 2
s_length = 3
# this is our input placeholder
input_img = Input(shape=(s_length,))
# "encoded" is the encoded representation of the input
encoded = Dense(encoding_dim, activation='relu')(input_img)
# "decoded" is the lossy reconstruction of the input
decoded = Dense(s_length, activation='sigmoid')(encoded)
# this model maps an input to its reconstruction
autoencoder = Model(input_img, decoded)
# this model maps an input to its encoded representation
encoder = Model(input_img, encoded)
# create a placeholder for an encoded (32-dimensional) input
encoded_input = Input(shape=(encoding_dim,))
decoder_layer = autoencoder.layers[-1]
# create the decoder model
decoder = Model(encoded_input, decoder_layer(encoded_input))

x_train = numpy.array(training_data)
print x_train[0]
autoencoder.compile(optimizer="adadelta", loss="binary_crossentropy")
autoencoder.fit(x_train, x_train, epochs=20, batch_size=256)

while True:
    iny = map(float, raw_input("> ").split(" "))
    print decoder.predict(encoder.predict([[iny]]))
