import math
import random
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib
import matplotlib.pyplot as plt
from scipy.optimize import minimize
import time

gantry_height = 600
dish_diam = 500
dish_radi = dish_diam/2
# p1,p2,p3, dish
colors = ["red", "blue", "green", "purple"]

# calculate pivot points
pivots = np.array([[0,0,gantry_height],
                   [0,0,gantry_height],
                   [0,0,gantry_height]])

tri_length = math.sqrt(3) * dish_radi
x_offset = tri_length/2
y_offset = (math.sqrt(3)/6) * tri_length

# x1,y1
pivots[0,0] = 0
pivots[0,1] = dish_radi
# x2,y2
pivots[1,0] = x_offset
pivots[1,1] = y_offset * -1
# x3,y3
pivots[2,0] = x_offset * -1 # below 0
pivots[2,1] = y_offset * -1

pvt1 = np.array([pivots[0,0], pivots[0,1], gantry_height])
pvt2 = np.array([pivots[1,0], pivots[1,1], gantry_height])
pvt3 = np.array([pivots[2,0], pivots[2,1], gantry_height])

print(pivots);

def distance(x1,y1,z1,x2,y2,z2):
    return math.sqrt((x1 - x2)**2 + (y1 - y2)**2 + (z1 - z2)**2)

def dish_pos_check(x,l1,l2,l3,tri_length):
    x1,y1,z1,x2,y2,z2,x3,y3,z3 = x

    p1 = np.array([x1, y1, z1])
    p2 = np.array([x2, y2, z2])
    p3 = np.array([x3, y3, z3])

    # two vectors in plane
    v1 = p3 - p1 #np.subtract([x3,y3,z3], [x1,y1,z1])
    v2 = p2 - p1 # np.subtract([x2,y2,z2], [x1,y1,z1])
    
    normal_vector = np.cross(v1,v2)
    
    z = normal_vector[2]
    
    #distance check of dish points
    dist1 = np.linalg.norm(p1 - p2) #distance(x1,y1,z1,x2,y2,z2)
    dist2 = np.linalg.norm(p1 - p3) #distance(x1,y1,z1,x3,y3,z3)
    dist3 = np.linalg.norm(p2 - p3) #distance(x2,y2,z2,x3,y3,z3)
    
    #distance check to pivot points x val       y val           z val
    dist1_to_p1 = np.linalg.norm(p1 - pvt1) #distance(x1,y1,z1,pivots[0,0],pivots[0,1],gantry_height)
    dist2_to_p2 = np.linalg.norm(p2 - pvt2) # distance(x2,y2,z2,pivots[1,0],pivots[1,1],gantry_height)
    dist3_to_p3 = np.linalg.norm(p3 - pvt3) #distance(x3,y3,z3,pivots[2,0],pivots[2,1],gantry_height)

    #z_val = 0
    #if z < 0:
    #    z_val = 1e6 # punish for noraml vector facing down
        
    edge_lengths = (dist1 - tri_length)**2 + (dist2 - tri_length)**2 + (dist3 - tri_length)**2 # lengths of the edge of "triangle" formed by x points
    
    potential_energy = ((z1 + z2 + z3)/3)**2 # average potential energy of all 3 points
    
    string_lengths = (dist1_to_p1 - l1)**2 + (dist2_to_p2 - l2)**2 + (dist3_to_p3 - l3)**2 # check how close our current string lengths are to what we want
    
    return edge_lengths + potential_energy + string_lengths #+ z_val


def get_x(l1, l2, l3, tri_length, x_start,  **kwargs):

    if x_start is None:
        x1 = random.uniform(pivots[2][0], pivots[1][0])
        x2 = random.uniform(pivots[2][0], pivots[1][0])
        x3 = random.uniform(pivots[2][0], pivots[1][0])
        y1 = random.uniform(pivots[2][1], pivots[0][1])
        y2 = random.uniform(pivots[2][1], pivots[0][1])
        y3 = random.uniform(pivots[2][1], pivots[0][1])
        z1 = random.uniform(0, gantry_height)
        z2 = random.uniform(0, gantry_height)
        z3 = random.uniform(0, gantry_height)
        x_start = [x1, y1, z1, x2, y2, z2, x3, y3, z3]
    
    d = minimize(dish_pos_check, x_start, method="L-BFGS-B", options={'maxiter': 500},args=(l1,l2,l3,tri_length,))
    return d.x
    
    #if ('plot' in kwargs):
        #if kwargs['plots']:
            
        #print("plotting")
    
    
    #angle = math.atan((y1-y2)/(x1-x2))
    #print(x1,y1,x2,y2, dmin, angle)
    
    
def asCartesian(rthetaphi):
    #takes list rthetaphi (single coord)
    r       = rthetaphi[0] 
    theta   = np.radians(rthetaphi[1])
    phi     = np.radians(rthetaphi[2])

    x = r * math.sin( theta ) * math.cos( phi )
    y = r * math.sin( theta ) * math.sin( phi )
    z = r * math.cos( theta )
    return [x,y,z]

def asSpherical(xyz):
    #takes list xyz (single coord)
    x       = xyz[0]
    y       = xyz[1]
    z       = xyz[2]
    r       =  math.sqrt(x*x + y*y + z*z) 
    theta   =  np.degrees(math.acos(z/r))
    phi     =  np.degrees(math.atan2(y,x))
    return [r,theta,phi]

def get_elaz(x):
    x1,y1,z1,x2,y2,z2,x3,y3,z3 = x

    # two vectors in plane
    normal_vector = get_norm(x)
    
    r, theta, phi = asSpherical(normal_vector)
    
    return [90 - theta, phi]

def vect_plot(ax, v1, color="red"):
    ax.quiver(0,0,0, v1[0], v1[1], v1[2], color=color)

def plot(x_val, pivots, l1,l2,l3,colors):
    x1,y1,z1,x2,y2,z2,x3,y3,z3 = x_val
    
    v1 = np.array([x1, y1, z1])
    v2 = np.array([x2, y2, z2])
    v3 = np.array([x3, y3, z3])
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    ax.scatter(pivots[0,0], pivots[0,1], pivots[0,2], color=colors[0])
    ax.scatter(pivots[1,0], pivots[1,1], pivots[1,2], color=colors[1])
    ax.scatter(pivots[2,0], pivots[2,1], pivots[2,2], color=colors[2])

    #ax.set_xlim(-300,300)
    #ax.set_ylim(-300,300)
    #ax.set_zlim(0,650)

    dish_x_vals = np.array([x1,x2,x3,x1]) # last entry for connecting of triangle
    dish_y_vals = np.array([y1,y2,y3,y1])
    dish_z_vals = np.array([z1,z2,z3,z1])
    
    # normal vector
    centroid = (v1 + v2 + v3)/3
    
    v13 = v3 - v1
    v12 = v2 - v1
    
    normal_vector = np.cross(v13,v12)/500.0
        
    print("normal vector bf: {}".format(normal_vector))
    
    vect_plot(ax, normal_vector, color="red")
    vect_plot(ax, v1, color="red")
    vect_plot(ax, v2, color="blue")
    vect_plot(ax, v3, color="green")
    vect_plot(ax, v12, color="red")
    vect_plot(ax, v13, color="red")


    ax.plot(dish_x_vals, dish_y_vals, dish_z_vals, color=colors[3])
    # string
    ax.plot([x1, pivots[0,0]], [y1, pivots[0,1]], [z1, pivots[0,2]], color=colors[0]) # l1
    ax.plot([x2, pivots[1,0]], [y2, pivots[1,1]], [z2, pivots[1,2]], color=colors[1]) # l2
    ax.plot([x3, pivots[2,0]], [y3, pivots[2,1]], [z3, pivots[2,2]], color=colors[2]) # l2

    #legend
    scatter1_proxy = matplotlib.lines.Line2D([0],[0], linestyle="none", c=colors[0], marker = 'o')
    scatter2_proxy = matplotlib.lines.Line2D([0],[0], linestyle="none", c=colors[1], marker = 'o')
    scatter3_proxy = matplotlib.lines.Line2D([0],[0], linestyle="none", c=colors[2], marker = 'o')
    ax.legend([scatter1_proxy, scatter2_proxy, scatter3_proxy], ['p1/l1 (l = {})'.format(l1), 'p2/l2 (l = {})'.format(l2), 'p3/l3 (l = {})'.format(l3)], numpoints = 1)
    
    a = 600
    ax.auto_scale_xyz([-a,a],[-a,a],[-a,a])
    
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')

    plt.show()
    #plt.savefig("{}{}{}_3D.jpg".format(l1,l2,l3))
    
def get_theta(vect_a, vect_b):

    
    dp = np.dot(vect_a, vect_b)
    theta = np.arccos(dp)
    
    return np.degrees(theta)

def get_norm(x):
    x1,y1,z1,x2,y2,z2,x3,y3,z3 = x
    
    v1 = np.array([x1, y1, z1])
    v2 = np.array([x2, y2, z2])
    v3 = np.array([x3, y3, z3])
    
    v13 = v3 - v1
    v12 = v2 - v1
    
    normal_vector = np.cross(v13,v12)
    
    return normal_vector
    

def get_length_1(guess_l, prev_l, desi_vect, tri_length, x_start):
    guess_l1, guess_l2, guess_l3 = guess_l
    prev_l1, prev_l2, prev_l3 = prev_l

    x = get_x(guess_l1, guess_l2, guess_l3, tri_length, x_start)
            
    el, az = get_elaz(x)
    cur_vect = asCartesian([1.0,el,az])
    
    dl1 = (prev_l1 - guess_l1)**2
    dl2 = (prev_l2 - guess_l2)**2
    dl3 = (prev_l2 - guess_l2)**2
    
    #print("========")
    #print("current:")
    #spheri_vect = asSpherical(cur_vect)
    #print(90-spheri_vect[0],spheri_vect[1])   
    #print("wanted:")
    #spheri_vect = asSpherical(desi_vect)
    #print(90-spheri_vect[0],spheri_vect[1])
    
    d_azel = (get_theta(desi_vect,cur_vect))
    #print("theta:")
    #print(d_azel)
    
    special = 0 # for punishment
    
    if min(guess_l1,guess_l2,guess_l3) < 0:
        special = 1e6
    
    total_diff = d_azel + ((dl1 + dl2 + dl3)/10000) + special#max(dl1, dl2)/2 #devalue difference because we value angle more

    return total_diff

def get_length(prev_l1, prev_l2, prev_l3, alt, azi, tri_length, x_start):
    
    desi_vect = asCartesian([1.0,alt,azi])
    
    #fig = plt.figure()
    #ax = fig.add_subplot(111, projection='3d')
    
    #vect_plot(ax, desi_vect) # for debugging
    #a = 1.5
    #ax.auto_scale_xyz([-a,a],[-a,a],[-a,a])
    #plt.show()
    
    print("Going for:")
    r, theta, phi = asSpherical(desi_vect)
    print(theta,phi)
    #print("Should be equal to:")
    #print(alt,azi)
    
    #time.sleep(5)
    
    
    l_start = [prev_l1, prev_l2, prev_l3]
    prev_l = [prev_l1, prev_l2, prev_l3]
    
    d = minimize(get_length_1, l_start, method = 'Nelder-Mead', args=(prev_l, desi_vect, tri_length, x_start), options={"maxiter": 300}) # LBthingy bounds = [(0,360),(0,360)],
    #print(d)
    #print("Difference: {}".format(d.fun))
    return d.x


## test of get x
#l1 = 10.0
#l2 = 15.0
#l3 = 400.0
#godel = 2**l1 * 3**l2 * 5**l3
#x_start = [pivots[0,0], pivots[0,1], gantry_height-10, pivots[1,0], pivots[1,1], gantry_height-10, pivots[2,0], pivots[2,1], gantry_height-10]
#x_val = get_x(l1,l2,l3, tri_length, x_start)
#print(get_elaz(x_val))
#plot(x_val,pivots,l1,l2,l3,colors)
#for i in range(3):
    #print("x{}: {}".format(i+1,x_val[i*3]))
    #print("y{}: {}".format(i+1,x_val[i*3+1]))
    #print("z{}: {}".format(i+1,x_val[i*3+2]))    
    
# test of get length
alt = 40
az = 0
phi_set = np.arange(0,360,2)
outfile = open("outputs.csv","w")
for phi in phi_set:
    l1 = 10
    l2 = 10
    l3 = 10
    x_start = [pivots[0,0], pivots[0,1], gantry_height-10, pivots[1,0], pivots[1,1], gantry_height-10, pivots[2,0], pivots[2,1], gantry_height-10]
    print(get_elaz(x_start))
    theta_set = np.arange(0,90,5)
    theta_set = theta_set[::-1] #start at top and work way down

    for theta in theta_set:
        print("=============")
        print("Theta: {}".format(theta))
        l1,l2,l3 = get_length(l1,l2,l3, theta, phi, tri_length, x_start)
        x_start = get_x(l1,l2,l3, tri_length, None)
        el, az = get_elaz(x_start)
        print("El, Az: {}, {}".format(el, az))
        print("Lengths: {}, {}, {}".format(l1,l2,l3))
        
        outfile.write("{},{},{},{},{}\n".format(el,az,l1,l2,l3))
        
        #plot(x_start, pivots, l1, l2, l3, colors)
    
outfile.close()
