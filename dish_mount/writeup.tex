\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{amsmath}

% Title Page
\title{A Dish Rotator for the masses}
\author{Linus Molteno}


\begin{document}
\maketitle

\begin{abstract}
I describe the versatile algorithm I've designed to allow a dish to be moved to point anywhere, while having an extremely simple mechanical design (described simply in Figure \ref{fig:dish1}) It uses generic inverse problem solutions, and although this might not be the most efficient solution to the problem, it is still very fast on a modern computer.
\end{abstract}

\tableofcontents

\section{Introduction}
Quite recently I’ve been playing around with listening to images from weather satellites using an omnidirectional antenna optimized for the frequency I’m listening to, 137 MHz. This antenna is designed to only listen upwards, which is ideal for satellites. The disadvantage for this is that it can only listen to relatively powerful signals from low-orbit satellites. These signals, because they’re at a low frequency and have low bandwidth, can’t transmit as much data as one in a higher frequency with higher bandwidth. The satellite signals at higher frequencies are often much harder to receive because they’re less powerful. In order to listen to these signals a high-gain antenna is needed. A simple way of making a high-gain antenna is putting the receiver at the focus of a reflective parabolic dish. These dishes effectively gather more radio waves from the satellite, and focus them to a point, where you put the antenna. However, because the satellites I want to listen to move, it’s necessary to have a way to point the dish to a specific place in the sky, and track the satellite. The standard way to do this is to have two motors at the bottom of the dish, one for each axis. This works very well with strong motors, and a lot of power, so this is what they use on telescopes such as the VLA\cite{napier1983very} in Figure~\ref{fig:vla}. Each of the 27 dishes used in the VLA telescope are 25m in diameter, weigh 209 tons and are supported using a massive mount as mentioned earlier. I am  aiming for a simpler, lighter design, one requiring less mechanical strength. So I have come up with a lightweight design to mount a dish up to several meters in diameter and allowing movement in all axes.
\begin{figure}
  \includegraphics[width=\linewidth]{VLA.jpg}
    \hspace*{15pt}\hbox{\scriptsize Credit:\thinspace{\small\itshape Wikimedia user 1Veertje}}

    \hspace*{15pt}\hbox{\scriptsize License:\thinspace{\scriptsize\itshape Creative Commons Attribution 2.0 Generic License}}

  \caption{The Karl G. Jansky Very Large Array in New Mexico, USA.}
  \label{fig:vla}
\end{figure}

\section{Mechanical design}

\begin{figure}[!ht]
    \begin{center}
        \includegraphics[width=\linewidth]{dish.png}
    \end{center}
    \caption{Simplified 3D render of one way of holding up the dish. The dish is approximated to part of a sphere rather than a paraboloid for this render.}
    \label{fig:dishrender}
\end{figure}

The design I've come up with involves 3 poles, placed anywhere (however an equilateral triangle is most efficient). A cable goes between the top of each pole and one of three points around the rim of the dish. The length of these cables will be adjusted by a winch to allow pointing anywhere. As long as the dish can be contained within the triangle formed by connecting these three points and that the points are sufficiently high above the ground to allow the dish to point at the horizon (that is, that the points' height is greater than the diameter of the dish). The point must also have sufficient depth available to be able to accomodate the full depth of the dish. A simple render of this is shown in Figure~\ref{fig:dishrender}. This could be very portable, assuming the poles can fold down, and are guyed to the ground, rather than permanently mounted.

\subsection{Dish Design}

For an appropriate dish (also portable), I have been experimenting with making one out of paper. If I can make the right template, I should be able to make it out of conductive cloth, which will reflect radio waves towards the recieving antenna. If it's just fabric, it could fold to something very small, and the bracing could be something like tent poles, where they just click together. Because both of these items are very light weight, the winches needed to lift it wouldn't need to be very strong. For a 3m dish (capable of not just listening to satellites, but pulsars too), I have a maximum estimate of about 5kg total weight. This is very lightweight compared to the galvanized steel of the commercial dishes. One disadvantage of using fabric and a hanging dish is that it wouldn't be able to withstand the wind, however if I were to use a mesh with larger gaps than the fabric, it would reduce windage and hopefully allow it to be out in more wind. But if it does get too windy, this system should be very easy to take down, and quite portable.

\subsection{Electrical Design}

This part of the design is pretty simple, just consisting of three motors and a controller. The controller has a few simple functions:
\begin{itemize}
 \item Listen to a computer over some serial link.
 \item Compute how far the motors should rotate given the lengths the computer sent
 \item Be able to home the system, allowing a reference point for later movements.
\end{itemize}
There is already free, open-source software for microcontrollers (such as an Arduino Mega) that fulfills all of these things, such as Marlin, used for 3D printers. This Arduino will be listening for Gcode. Gcode is a code used for a variety of CNC machines, and basically says 'Move X to here, Y to here, Z to here' and is sent sequentially to tell the machine to follow a predefined pattern to produce a part. If I assign each axis to one of the motors, and thereby one of the cables, I can have the computer easily control the length of the cables by the XYZ position given to the Arduino.

The motors I'm using are stepper motors. These are accurate motors, that rotate in small steps of rotation, something like 1$^{\circ}$ at a time. These motors will have spools attached, where the cable will be kept. If the diameter of this spool is known, and it is long enough to allow the cable to go along its full width without layering, the length of the wire would be accurate. These motors should be able to provide more than enough force to lift a heavy dish, especially if the load is shared between three of them. In order to drive these motors, it requires a special driver. These drivers are commercially available, and can be used in conjunction with what's called a RAMPS 1.4 board. This board is designed for 3D printers, and has the ability to hold 4 stepper motor drivers (X, Y, Z, extruder), and plugs directly onto an Arduino Mega. Using the software I mentioned earlier (Marlin), these are very easy to control.

\subsection{Prototype}
For a small-scale prototype, using a subsitute dish 500mm in diameter (a circle), I decided to make a triangle, where the cables that hold up the dish are fishing line, going through grooves cut in wood halfway along each side. The triangle is raised up 500mm, and adequately strengthened with cross bracing.

\section{Control Algorithm}

The algorithm I’ve designed is about as generic as I can make it. Given XYZ coordinates for each of the points $p_{1}$, $p_{2}$, and $p_{3}$ and the diameter of the dish, it can calculate the lengths of the cables required to point anywhere in the sky. I use a technique of solving inverse problems.

\begin{figure}[!ht]
    \begin{center}
        \includegraphics[width=0.3\linewidth]{robot}
    \end{center}
    \caption{A simple two-armed robot. $p_{1}$ is fixed.}
    \label{fig:robot}
\end{figure}

\subsection{Example of Inverse Problem}
A classic example of this type of algorithm is a two armed robot in two dimensions, described in Figure \ref{fig:robot}. $p_{1}$ and $p_{2}$ both have motors at their points, adjusting the angle of $l_{1}$ and $l_{2}$ respectively. The goal of the algorithm is to be able to move $p_{3}$ to $p_{target}$. This is done by adjusting the values of $\theta_{1}$ and $\theta_{2}$. In order to do this, first there needs to be a way of calculating the position of $p_{3}$ based on $\theta_{1}$ and $\theta_{2}$. This is called solving the forward problem, and can be done analytically, where...

$$p_{3} = \Big(l_{1}sin(\theta_{1}) + l_{2}sin(\theta_{2}),  l_{1}cos(\theta_{1}) + l_{2}cos(\theta_{2}) \Big) $$

Now that I have this, I can calculate a score for that position of $p_{3}$, and because $l_{1}$ and $l_{2}$ are fixed, the score of that position depends only on $\theta_{1}$ and $\theta_{2}$. This will be important later. The score for the position of $p_{3}$ is dependent on the distance to $p_{target}$. The smaller the distance or score, the better the position.

So if I write a function that takes in $\theta_{1}$ and $\theta_{2}$, and returns the distance between $p_{3}$ and $p_{target}$, I can use a minimiser on that function to find the optimum values of $\theta_{1}$ and $\theta_{2}$. A minimiser is a function that finds these optimal values. It is given a function (this is the function I just wrote) which returns a score, and tries to find whatever input values to the given function have the smallest output, or score. And there you go! Now you have solved the inverse problem. If you were to run that minimiser with different $p_{target}$s and you can move it however you like.

\begin{figure}[!ht]
    \includegraphics[width=\linewidth]{dish_diagram}
    \caption{Top down diagram of the rotator}
    \label{fig:dish1}
\end{figure}

\subsection{Forward Problem}

This function, when given the length of the three cables ($l_{1}$, $l_{2}$, and $l_{3}$), will calculate the position and rotation of the dish. This can’t be analytically solved simply like the previous example, so I have to use an inverse solution to the forward part of the bigger problem. In order to do this, I created a function which, when given guesses for $d_{1}$, $d_{2}$, and $d_{3}$, calculates the values relating to the position and size of the dish, then calculates a score for those points. This score can be represented by this set of equations:

$$ S_{x} = (x_{1} - l_{t})^{2} + (x_{2} - l_{t})^{2} + (x_{3} - l_{t})^{2} $$

This part determines how far the points $d_{1}$, $d_{2}$, and $d_{3}$ are from forming an equilateral triangle in 3D space, where $l_{t} = \sqrt{3} \times \frac{D}{2}$ where $D$ is the diameter of the dish. This is the ideal length of $x_{1}$.

$$ S_{l} = (l_{1} - l_{1d}) + (l_{2} - l_{2d}) + (l_{3} - l_{3d}) $$


This part determines how far $l_{1}$, $l_{2}$, and $l_{3}$ are from the correct length, where $l_{1d}$ is the desired length of $l_{1}$.

$$ S_{E_{p}} = \left(\frac{d_{1z} + d_{2z} + d_{1z}}{3}\right)^{2} $$

This part calculates the average potential energy of the dish. This is done in order to prevent the dish from being inverted. It effectively simulates gravity, forcing the minimiser to find the lowest possible dish that fits all the other criteria. In this equation, $d_{1z}$ is the Z component of $d_{1}$.

$$ S = S_{x} + S_{l} + S_{E_{p}} $$

Now that I have a score based on a guess of $d_{1}$, $d_{2}$, and $d_{3}$, I can minimise the score using a minimiser similar to the one I described earlier (solving the inverse problem of the forward problem).
As this code is written in a programming language called Python, I can use SciPy\footnote{SciPy library of code for Python which does a variety of computations relating to science (mostly physics). It has many parts, but I'm using the 'optimisation' module. I also used SciPy for the vector math (from the 'linear algebra' module), as it has fast ways of doing those things.}, which has a variety inbuilt optimisation algorithm (I ended up using 'L-BFGS-B') to find the position of the dish, given $l_{1}$, $l_{2}$, and $l_{3}$. This operation takes very little time on a modern computer (24ms).

\subsection{Inverse Problem}
This function is given a position in the sky that the user wants the dish to point to. It uses a similar technique to figure out the length of $l_{1}$, $l_{2}$, and $l_{3}$ required to point there. This is the main functionality of the algorithm. Rather than starting with a random length of $l_{1}$, $l_{2}$, and $l_{3}$, it uses the current lengths as a starting point in order to minimise time, as it is rare that it'll have to compute a position halfway across the sky if it's just tracking something that's moving relatively slowly. The lengths are scored by this set of equations:

$$S_{pointing} = \left| \vec{a_{desired}} - \vec{a_{current}} \right|^{2}$$

This returns the length of the vector separating the current vector ($\vec{a_{current}}$) and the desired vector ($\vec{a_{desired}}$). This represents the pointing error, hence $S_{pointing}$. This is how far the dish is from pointing in the right direction. This is squared in order to value it more.

$$S_{d} = \frac{(l_{1} - l_{1prev})^{2} + (l_{2} - l_{2prev})^{2} + (l_{3} - l_{3prev})^{2}}{10000} $$
How far each of these lengths are from their previous length. Each of the individual differences is squared to do two things. First, if it's negative, it will become positive. Second, it means that big differences matter more than little differennces. This is divided by 10000 because I value the angular pointing more, and don't want to detract from that.

$$S = S_{pointing} + S_{d} + S_{special}$$

If the dish goes below the ground, I set $S_{special}$ to be $10^{6}$, effectively disqualifying these lengths, otherwise $S_{special} = 0$. I check if the dish is below the ground by seeing if the Z component of any of $d_{1}$, $d_{2}$, or $d_{3}$ is $< 0$.

Again this score is minimised using SciPy’s inbuilt module, however this time I used the 'Nelder-Mead' method. The time it takes to do this operation depends on how far you’re aiming to go from your current position (as it uses the current position as a starting point), but on average it takes a couple of seconds. I've set the maximum number of iterations (attempts at the smallest score) to 500, so the maximum time it would take is 23ms$\times 500$ iterations, or 11.5s total.

\subsection{Other Optimizations}

Although this algorithm can compute a new, nearby, set of lengths in a short time, I decided to try and optimise it as much as possible. One of these optimsations was what's called caching the result. It basically is saving the result in a database. So once you've specified the dimensions of your system (the positions of $p_{1}$, $p_{2}$, and $p_{3}$, and the diameter of the dish, $D$), every position in the sky will be assigned a set of three lengths, the lengths of the cables. If the position you want to go to is nearby a previously saved position, you could just get those lengths without having to compute it every time.

A possible future optimisation is using a different programming language. Python isn't a very fast language for many reasons, but it's very easy to read and write. Switching to another language, like Rust or similar, should give a large speed up, however the issue with switching to languages like that is that they don't always have as good libraries for this kind of thing. Python has been around long enough to have gained a good following of people in the community of people who use this it for the sort of thing I'm doing, so people write code and release it online to help people write this sort of thing.

\begin{figure}[!ht]
    \includegraphics[width=\linewidth]{algorithm1}
    \caption{The result. I have asked the algorithm to point to 80$^{\circ}$ above the horizon, straight north. The purple triangle represents the dish.}
    \label{fig:algorithm1}
\end{figure}

\section{Integration into tracking software}

While I can use this software as is, manually entering in positions, it is optimal to use some tracking software. There are many libraries for doing these sorts of orbital calculations, I've grown used to one called GPredict, which also does the calculations for doppler shift and allows control of my favourite radio control program, GQRX. The way it communicates with external programs (like the one I'm aiming to write) is through a TCP socket. Assuming I can create a TCP socket that can listen to what GPredict is saying, I should be able to tell where to point for any satellite. Using the Python package called 'socket', it's about 30 lines of code to open one up and parse the messages.

\bibliographystyle{plain}
\bibliography{dish}
\end{document}
