from scipy.interpolate import RegularGridInterpolator
import csv
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib
import matplotlib.pyplot as plt

csvfile = open('outputs.csv', 'rb')
reader = csv.reader(csvfile, delimiter=',', quotechar=',')

l1 = []#np.array([],ndmin=2)
l2 = []#np.array([],ndmin=2)
l3 = []#np.array([],ndmin=2)

for row in reader:

    el = float(row[0])
    az = float(row[1])
    tmp_l1 = float(row[2])
    tmp_l2 = float(row[3])
    tmp_l3 = float(row[4])
    l1.append([el,az,tmp_l1])
    l2.append([el,az,tmp_l2])
    l3.append([el,az,tmp_l3])

l1 = np.array(l1)
l2 = np.array(l2)
l3 = np.array(l3)
csvfile.close()

X = []
Y = []
Z = []
for i in l1:
    if i[0] > 3:
        X.append(i[0])
        Y.append(i[1])
        Z.append(i[2])

## create 2d x,y grid (both X and Y will be 2d)
#X, Y = np.meshgrid(l1[:,0], l1[:,1])

## repeat Z to make it a 2d grid
#Z = np.tile(l1[:,2], (len(l1[:,2]), 1))

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

ax.scatter(X, Y, Z) 

ax.set_xlabel('Elevation')
ax.set_ylabel('Azimuth')
ax.set_zlabel('l1')
plt.title("l1")

plt.show()
