import util
import json

class Movement:
    def __init__(self, owner, originXY, destinationXY, number_of_ships, distance, board): # dist in number of turns
        self.owner = owner
        self.origin = originXY # tuple
        self.destination = destinationXY # tuple
        self.number_of_ships = number_of_ships
        self.total_distance = distance # in no of turns
        self.progress_distance = 0
        originPlanet = board.getPlanetByXY(self.origin[0], self.origin[1])
        if originPlanet:
            board.changeShips(-number_of_ships, originPlanet)
    
    def boardChange(self, board):
        originPlanet = board.getPlanetByXY(self.origin[0], self.origin[1])
        board.changeShips(-self.number_of_ships, originPlanet)
        return board
        
    def turn(self, board, log):
        print("Yeah nah turning")
        self.progress_distance += 1
        if self.progress_distance == self.total_distance:
            originX = self.origin[0]
            originY = self.origin[1]
            destinX = self.destination[0]
            destinY = self.destination[1]
            
            tmpOrigin = board.getPlanetByXY(originX, originY)
            tmpDest = board.getPlanetByXY(destinX, destinY)
            
            destPlayerText = util.playerText(tmpDest.owner, True)
            originPlayerText = util.playerText(self.owner, True)
                        
            if tmpDest.owner == self.owner:
                board.changeShips(self.number_of_ships, tmpDest)
                log += tmpDest.name + " has been reinforced by " + str(self.number_of_ships) + " ships\n"
            elif tmpDest.owner != self.owner:
                shipsLeft = int(tmpDest.ships - (tmpOrigin.killPercentage * self.number_of_ships))
                if shipsLeft < 0: # attacker won
                    log += originPlayerText + " has captured " + tmpDest.name + " from " + destPlayerText + "\n"
                    board.changeOwner(self.owner, destinX, destinY)
                    board.changeShipsAbs(-shipsLeft, tmpDest)
                else: # defender won
                    log += destPlayerText + " successfully defended against " + originPlayerText + " on " + tmpDest.name + "\n"
                    board.changeShipsAbs(shipsLeft, tmpDest)
            
            return True, log, board
        else:
            return False, log, board
        
    def toJSON(self):
        inDict = {'origin' : self.origin, 'owner' : self.owner, 'destination' : self.destination, 'ships' : self.number_of_ships, 'total_distance' : self.total_distance, 'progress_distance' : self.progress_distance}
        
        return json.dumps(inDict)
    
    def fromJSON(self, injson):
        injson = json.loads(injson)
        self.origin = injson['origin']
        self.owner = injson['owner']
        self.destination = injson['destination']
        self.number_of_ships = injson['ships']
        self.total_distance = injson['total_distance']
        self.progress_distance = injson['progress_distance']
        
def printMovements(movements, board):
    for i in movements:
        originName = board.getPlanetByXY(i.origin[0], i.origin[1]).name
        destinName = board.getPlanetByXY(i.destination[0], i.destination[1]).name
        
        current_percent = int((i.progress_distance / i.total_distance) * 100)
        remaining = int(i.total_distance - i.progress_distance)
        
        print(str(i.number_of_ships) + " ships: " + originName + " +" + "-" * int((current_percent - 1)/2) + "o" + "-" * int((100 - (current_percent + 1))/2) + "+ " + destinName + " | " + str(remaining) + " turns remaining\n")
