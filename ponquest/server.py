from planet import Planet
from board import Board
from movement import Movement, printMovements
from bcolours import Bcolours
from math import sqrt
import subprocess
import util
from flask import Flask
from flask_restful import Api, Resource, abort, reqparse
import time
from pymongo import MongoClient
from multiprocessing import Process
import json
import argparse

client = MongoClient()

db = client.ponquest

app = Flask(__name__)
api = Api(app)

def getDistance(x,y):

    xDiff = x[0] - x[1]
    yDiff = y[0] - y[1]
    
    distance = int(sqrt((xDiff)**2 + (yDiff)**2)) # rounded down
    if distance == 0:
        distance = 1
    return distance

def printAll(turnCounter, board, curPlayer, movements, log):
    subprocess.run("clear")
    subprocess.run("clear")

    board.printBoard()
    
    print("Turn " + str(turnCounter))
    
    with open("log.txt", "wt") as fn:
        if log[-1] == ":":
            log += "\n"
        fn.write(log + "\n")
    
    playerText = util.playerText(curPlayer, False)
    
    print(playerText + "'s Turn" + Bcolours.ENDC)
                    
    ownedPlanets = board.getPlanetsByOwner(curPlayer)
    ownedPlanetsNumbers = []
    for i in ownedPlanets:
        ownedPlanetsNumbers.append(i.name[7:])
    planetsOwnedText = "You own planet(s) "
    planetsOwnedText += ", ".join(ownedPlanetsNumbers)
    planetsOwnedText += "\n"
    print(planetsOwnedText)
    
    ownedMovements = []
    
    for i in movements:
        if i.owner == curPlayer:
            ownedMovements.append(i)
    
    printMovements(ownedMovements, board)

def changeBoard(boardJSON):
    boardColl = db.board
    newBoard = {'boardJSON': boardJSON}
    boardColl.delete_many({})
    boardColl.insert_one(newBoard)
    
def getBoard():
    boardColl = db.board
    boardJSON = boardColl.find_one()['boardJSON']
    tmpBoard = Board(5,5,[Planet("test", False)])
    tmpBoard.fromJSON(boardJSON)
    
    return tmpBoard

def changeMovements(movementJSON):
    movementJSON2 = []
    for i in movementJSON:
        movementJSON2.append(i.toJSON())
    movementsColl = db.movement
    newMovements = {'movements': movementJSON2}
    movementsColl.delete_many({})
    movementsColl.insert_one(newMovements)
    
def getMovements():
    movementsColl = db.movement
    movementJSON = movementsColl.find_one()
    movementArr = movementJSON['movements']
    
    outMovements = []
    
    for i in movementArr:
        tmpMovement = Movement(0, [0,0], [1,1], 1, 1, getBoard())
        tmpMovement.fromJSON(i)
        outMovements.append(tmpMovement)
            
    return outMovements
    
def changePlayer(inPlayer):
    print("changing player to {}".format(inPlayer))
    playerColl = db.player
    newPlayer = {'player': inPlayer}
    playerColl.delete_many({})
    playerColl.insert_one(newPlayer)
    
def getPlayer():
    playerColl = db.player
    player = playerColl.find_one()
    return player['player']

def setTurnComplete(yesno):
    turnColl = db.turn
    newTurn = {'turn': yesno}
    turnColl.delete_many({})
    turnColl.insert_one(newTurn)
    
def getTurnComplete():
    turnColl = db.turn
    turnJSON = turnColl.find_one()
    try:
        return turnJSON['turn']
    except:
        return False

parser = reqparse.RequestParser()
parser.add_argument('movements')

class getMove(Resource):
    def get(self, player):
        board = getBoard()
        if player > numPlayers:
            abort(404, "Please make sure your player ID is in current players")
        if player == getPlayer(): # player's turn
            movementArr = []
            for i in getMovements():
                movementArr.append(i.toJSON())
            return {"turn": True, "board" : board.toJSON(), "movements" : movementArr}
        else:
            return {"turn": False, "board" : board.toJSON(), "movements" : False}
        
class postMovement(Resource):
    def post(self, player):
        args = parser.parse_args()
        added_movements = json.loads(args['movements'])
        tmpMovements = getMovements()
        if player == getPlayer():
            for i in added_movements:
                decodedMovement = Movement(0, [0,0], [1,1], 1, 1, getBoard())
                decodedMovement.fromJSON(i)
                changeBoard(decodedMovement.boardChange(getBoard()).toJSON())
                x,y = decodedMovement.origin
                tmpPlanet = getBoard().getPlanetByXY(x,y)
                if tmpPlanet:
                    if tmpPlanet.owner == player:
                        tmpMovements.append(decodedMovement)                    
                        changeMovements(tmpMovements)
                        setTurnComplete(True)
                    else:
                        return {"error": "You don't own that planet"}
                else:
                    return {"error": "That planet doesn't exist"}
            setTurnComplete(True)
            
                
movements = []

curPlayer = 1

api.add_resource(getMove, '/move/<int:player>')
api.add_resource(postMovement, '/movements/<int:player>')

def runServer():
    app.run(host="0.0.0.0")
    
log = ""

def runGame():
    isWon = False

    
    turnCounter = 0
        
    while not isWon:
        
        turnCounter += 1
        
        print("Beginning of turn {}".format(turnCounter))
        tmpboard = getBoard()
        
        if tmpboard.turn():
            isWon = True
            
        newMovements = []
        oldMovements = getMovements()
        
        print(oldMovements)
        
        for move in oldMovements:
            log = ""
            done, log, tmpboard = move.turn(tmpboard, log)
            if not done:
                newMovements.append(move)
        
        changeBoard(tmpboard.toJSON())
        changeMovements(newMovements)
            
        for i in range(numPlayers):
            setTurnComplete(False)
            
            i += 1
            
            changePlayer(i)
            
            while not getTurnComplete():
                time.sleep(0.1)
            print("SOMEONE DID A DO")

                    
                    
if __name__ == '__main__':
    
    argparser = argparse.ArgumentParser(description='Run Ponquest server.', 
                                    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    argparser.add_argument('--height', required=False, default=5, help="Height of board")
    argparser.add_argument('--width', required=False, default=10, help="Width of board")
    argparser.add_argument('--players', required=False, default=2, help="Number of players.")
    argparser.add_argument('--planets', required=False, default=15, help="Number of planets.")

    ARGS = argparser.parse_args()
    
    #names = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "Jimmy", "Bob", "Geoffrey", "Joe"]
    names = []
    namePrefix = "planet_"
    
    height = ARGS.height
    width = ARGS.width
    numPlayers = ARGS.players

    numPlanets = ARGS.planets
    
    for i in range(100):
        name = str(i)
        names.append(namePrefix + name.zfill(2))
        
    planets = []

    for i in range(numPlayers):
        name = names[i]
        tmpPlanet = Planet(name, False)
        tmpPlanet.owner = i + 1
        planets.append(tmpPlanet)

    for i in range(numPlanets-numPlayers):
        name = names[i + numPlayers]
        planets.append(Planet(name, True))
        
    board = Board(width, height, planets)
    
    changeMovements([])
    changePlayer(1)
    changeBoard(board.toJSON())
    setTurnComplete(False)
    p = Process(target=runGame)
    p1 = Process(target=runServer)
    p.start()
    p1.start()
    p.join()
