from planet import Planet
from board import Board
from movement import Movement, printMovements
from bcolours import Bcolours
from math import sqrt
import util
import requests
import json
import time
import subprocess
import argparse

namePrefix = "planet_" # do not remove

fps = 60
separator_length = 3

def getDistance(x,y):

    xDiff = x[0] - x[1]
    yDiff = y[0] - y[1]
    
    distance = int(sqrt((xDiff)**2 + (yDiff)**2)) # rounded down
    if distance == 0:
        distance = 1
    return distance

def printAll(turnCounter, board, curPlayer, movements):
    subprocess.run("clear")
    subprocess.run("clear")

    board.printBoard()
    
    print("Turn " + str(turnCounter))
    
    playerText = util.playerText(curPlayer, False)
        
    print(playerText + "'s Turn" + Bcolours.ENDC)
                    
    ownedPlanets = board.getPlanetsByOwner(curPlayer)
    ownedPlanetsNumbers = []
    for i in ownedPlanets:
        ownedPlanetsNumbers.append(i.name[7:])
    planetsOwnedText = "You own planet(s) "
    planetsOwnedText += ", ".join(ownedPlanetsNumbers)
    planetsOwnedText += "\n"
    print(planetsOwnedText)
    
    ownedMovements = []
    
    for i in movements:
        if i.owner == curPlayer:
            ownedMovements.append(i)
    
    printMovements(ownedMovements, board)

board = Board(5,5,[Planet("test", False)])

def getMove(server_dns,player):
    r = requests.get(server_dns + "move/" + str(player))

    resp = r.json()

    boardJSON = resp['board']
    
    tmpBoard = Board(5,5,[Planet("test", False)])
    tmpBoard.fromJSON(boardJSON)
    
    movements = False
    
    if resp['movements']:
        movements = []
        inJSON = resp['movements']
        for i in inJSON:
            tmpMovement = Movement(0, [0,0], [1,1], 1, 1, tmpBoard)
            tmpMovement.fromJSON(i)
            movements.append(tmpMovement)
    
    return resp['turn'], tmpBoard, movements

def postMovements(movements, server_dns, player):
    moveJSON = []
    for i in movements:
        moveJSON.append(i.toJSON())
        
    moveJSON = json.dumps(moveJSON)
    
    r = requests.post(server_dns + "movements/" + str(player), data = {'movements' : moveJSON})
    
    try:
        r.json()['error']
        return False
    except:
        return True
    
def runAnim(filename, board):
    with open(filename, 'rt') as anim:
        lines =  anim.readlines()
        separator_count = 0
        framebowl = ""
        for i in lines:
            if i == "\n":
                separator_count += 1
            else:
                framebowl += i
            if separator_count == separator_length:
                subprocess.run("clear")
                separator_count = 0
                board.printBoard()
                print(Bcolours.HEADER + "Please enjoy your stay in the 'Not Your Bloody Turn, Dimbo' waiting room, dimbo." + Bcolours.ENDC)
                print(framebowl)
                framebowl = ""
                time.sleep(1/fps)
    
turnCounter = 0

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Play Ponquest.', 
                                    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--ip', required=False, default='http://192.168.0.140:5000/', help="Server URL.")
    parser.add_argument('--player', required=False, default=1, help="Player number.")

    ARGS = parser.parse_args()
    
    server_dns = ARGS.ip
    player = int(ARGS.player)

    while True:
                    
        newMovements = []
                
        turn, board, movements = getMove(server_dns, player)

        
        if not movements:
            movements = []
        
        if turn:
            turnCounter += 1

            printAll(turnCounter, board, player, movements)
            
            satisfied = False
            extraSatisfied = False
            
            movementError = False

            while not satisfied:
                    
                print("=" * 10)
                inny = input("type 'mv <origin planet> <destination planet> <number of ships>'\nor type 'dist <origin planet> <destination planet>'\nor type 'info <planet>' \nor press enter to end turn now\n")
                if "mv" in inny:
                    innySplit = inny.split(" ")
                    if len(innySplit) != 4:
                        print(Bcolours.FAIL + Bcolours.UNDERLINE + Bcolours.BOLD + "mv takes 3 arguments" + Bcolours.ENDC)
                    else:
                        numberShips = innySplit[3]
                        originName = innySplit[1]
                        destinName = innySplit[2]
                        
                        originPlanet = board.getPlanetByName(namePrefix + originName)
                        destinPlanet = board.getPlanetByName(namePrefix + destinName)
                        
                        if originPlanet and destinPlanet:
                            try:
                                numberShips = int(numberShips)
                                originXY = board.getXY(originPlanet)
                                destinXY = board.getXY(destinPlanet)
                                
                                
                                xArray = [originXY[0], destinXY[0]]
                                yArray = [originXY[1], originXY[1]]
                                
                                distance = getDistance(xArray, yArray)
                                
                                if originPlanet.owner == player:
                                    if originPlanet.ships >= int(numberShips):

                                        tmpMovement = Movement(player, originXY, destinXY, numberShips, distance, board)
                                        newMovements.append(tmpMovement)
                                        
                                        printAll(turnCounter, board, player, movements + newMovements)
                                    else:
                                        print(Bcolours.FAIL + Bcolours.UNDERLINE + Bcolours.BOLD + "Not enough ships on the origin planet" + Bcolours.ENDC)
                                else:
                                    print(Bcolours.FAIL + Bcolours.UNDERLINE + Bcolours.BOLD + "You do not own the origin planet" + Bcolours.ENDC)
                            except ValueError:
                                print(Bcolours.FAIL + Bcolours.UNDERLINE + Bcolours.BOLD + "Number of ships must be an integer" + Bcolours.ENDC)
                elif "dist" in inny:
                    innySplit = inny.split(" ")
                    if len(innySplit) != 3:
                        print(Bcolours.FAIL + Bcolours.UNDERLINE + Bcolours.BOLD + "dist takes 2 arguments" + Bcolours.ENDC)
                    else:
                        originName = innySplit[1]
                        destinName = innySplit[2]
                        
                        originXY = board.getXYByName(namePrefix + originName)

                        destinXY = board.getXYByName(namePrefix + destinName)

                        xArray = [originXY[0], destinXY[0]]
                        yArray = [originXY[1], destinXY[1]]
            
                        printAll(turnCounter, board, player, movements + newMovements)
                        
                        if originXY and destinXY:
                            print(Bcolours.BOLD + "The distance is " + str(getDistance(xArray, yArray)) + Bcolours.ENDC)
                        else:
                            print(Bcolours.FAIL + Bcolours.UNDERLINE + Bcolours.BOLD + "Those planets do not exist" + Bcolours.ENDC)
                elif "info" in inny:
                    innySplit = inny.split(" ")
                    if len(innySplit) != 2:
                        print(Bcolours.FAIL + Bcolours.UNDERLINE + Bcolours.BOLD + "info takes in 1 argument" + Bcolours.ENDC)
                    else:
                        originName = innySplit[1]
                        originPlanet = board.getPlanetByName(namePrefix + originName)
                        
                        printAll(turnCounter, board, player, movements)
                        
                        if originPlanet:
                            print(Bcolours.BOLD + namePrefix + originName + ":" + Bcolours.ENDC)
                            print("Owner: " + str(originPlanet.owner))
                            print("Kill%: " + str(originPlanet.killPercentage))
                            print("ProdRate: " + str(originPlanet.prodrate))
                elif inny == "":
                    postMovements(newMovements, server_dns, player)
                    satisfied = True
                    turn = False
                    time.sleep(0.5)
        else:
            subprocess.run("clear")
            subprocess.run("clear")
            #board.printBoard()
            print(Bcolours.HEADER + "Other person's turn" + Bcolours.ENDC)
            runAnim("../asciiAnimation/out.txt", board)
