import random
from util import killRandom

class Planet:
    def __init__(self, name, neutral):
        self.name = name
        
        self.killPercentage = killRandom()
        
        if neutral:
            self.prodrate = 1
            self.ships = 0
            self.owner = 0
        else:
            self.prodrate = 10#int((random.normalvariate(0.5, 0.1) * 15) + 5)
            self.killPercentage = 0.4
            self.ships = 0
        
            
    def turn(self):
        self.ships += self.prodrate
        
    def changeOwner(self, owner):
        print(self.owner, owner)
        if self.owner == 0 and owner != 0:
            print("Changing prodrate")
            self.prodrate = int((random.normalvariate(0.5, 0.1) * 15) + 5)
        self.owner = owner

