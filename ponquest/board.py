import random
from bcolours import Bcolours
from planet import Planet
import json
import util

class Board:
    def __init__(self, width, height, planets):
        # create board itself
        self.twodBoard = [[0 for i in range(height)] for i in range(width)]
        
        self.height = height
        self.width  = width
        
        # place planets on board
        tmp_board = [i for i in range(width*height)]
            
        select_few = random.sample(tmp_board, len(planets))
        
        for i in select_few:
            planet = planets[select_few.index(i)]
            x = i % self.width
            y = int(i/self.width)
            self.twodBoard[x][y] = planet
            
        # get maximum length of planet name
        nameLens = []
        for i in planets:
            nameLens.append(len(i.name))
        
        self.maxNameLength = max(nameLens) + 1
            
    def getPlanetByXY(self, x, y):
        planet = self.twodBoard[x][y]
        if planet == 0:
            return False
        else:
            return planet
        
    def getXYByName(self, name):
        index = (-1,-1)
        for y in range(self.height):
            for x in range(self.width):
                planet = self.getPlanetByXY(x,y)
                if planet:
                    if planet.name == name:
                        index = (x,y)
                    
        
        if index == (-1, -1):
            return False
        else:
            return index
        
    def getPlanetByName(self, name):
        index = self.getXYByName(name)
        if index:
            return self.getPlanetByXY(index[0], index[1])
        
    def getXY(self, planet):
        index = (-1,-1)
        for y in range(self.height):
            for x in range(self.width):
                tmp_planet = self.getPlanetByXY(x,y)
                if self.getPlanetByXY(x, y):
                    if tmp_planet == planet:
                        index = (x, y)
                    
        
        if index == (-1, -1):
            return False
        else:
            return index
        
    def turn(self):
        owners = []
        print("turnt")
        for x in range(self.width):
            for y in range(self.height):
                tmp = self.getPlanetByXY(x,y)
                if tmp:
                    # it is a planet
                    tmp.turn()
                    if tmp.owner != 0:
                        owners.append(tmp.owner)
        
        return util.checkEqual(owners)            
            
    def getPlanetsByOwner(self, owner):
        planets = []
        for y in range(self.height):
            for x in range(self.width):
                tmp = self.getPlanetByXY(x,y)
                if tmp:
                    if tmp.owner == owner:
                        planets.append(tmp)
                        
        return planets
            
    
    def changeOwner(self, owner, x, y):
        planet = self.twodBoard[x][y]
        if planet == 0:
            return False
        else:
            self.twodBoard[x][y].changeOwner(owner)
            
    def changeShips(self, number, planet):
        if self.getXY(planet):
            x, y = self.getXY(planet)
            self.twodBoard[x][y].ships += number
        else:
            print("planet {} no existo" + planet.name)
        
    def changeShipsAbs(self, number, planet):
        x, y = self.getXY(planet)
        self.twodBoard[x][y].ships = number
            
    def printBoard(self):
        topbottom = "-" + ("-" * self.maxNameLength) + "+"
        mid1 = "|"
        mid2 = "|"
        
        print("+" + (self.width*topbottom))
        
        for y in range(self.height):
            namebowl = mid1
            shipbowl = mid1
            ownerbowl = mid1
            for x in range(self.width):
                planet = self.getPlanetByXY(x,y)
                name = " "
                numberShips = 0
                owner = " "
                if planet:
                    numberShips = str(planet.ships)
                    name = planet.name
                    owner = str(planet.owner)
                else:
                    name = " "
                    numberShips = " "
                  
                # for name                  
                lendiff = self.maxNameLength - len(name)
                
                namediff1 = 0
                namediff2 = 0
                
                if lendiff != 0:
                    namediff1 = int(lendiff/2)
                    namediff2 = (lendiff - namediff1)
                                
                # for number of ships
                lendiff = self.maxNameLength - len(numberShips)
                
                shipdiff1 = 0
                shipdiff2 = 0
                
                if lendiff != 0:
                    shipdiff1 = int(lendiff/2)
                    shipdiff2 = (lendiff - shipdiff1)
                                
                
                # for owner
                lendiff = self.maxNameLength - len(owner)
                
                diff1 = 0
                diff2 = 0
                
                if lendiff != 0:
                    diff1 = int(lendiff/2)
                    diff2 = (lendiff - diff1)
                
                fillChar = " "
                
                if owner == "1":
                    namebowl += Bcolours.PLAY1 +  (" " * (namediff1 + 1)) + name + (" " * namediff2) + Bcolours.ENDCBLOCK + mid2
                    shipbowl += Bcolours.PLAY1 + (" " * (shipdiff1 + 1)) + numberShips + (" " * (shipdiff2)) + Bcolours.ENDCBLOCK + mid2
                    ownerbowl += Bcolours.PLAY1 + (fillChar * (diff1 + 1)) + owner + (fillChar * diff2) + Bcolours.ENDCBLOCK + mid2
                elif owner == "2":
                    namebowl += Bcolours.PLAY2 +  (" " * (namediff1 + 1)) + name + (" " * namediff2) + Bcolours.ENDCBLOCK + mid2
                    shipbowl += Bcolours.PLAY2 + (" " * (shipdiff1 + 1)) + numberShips + (" " * (shipdiff2)) + Bcolours.ENDCBLOCK + mid2
                    ownerbowl += Bcolours.PLAY2 + (fillChar * (diff1 + 1)) + owner + (fillChar * diff2) + Bcolours.ENDCBLOCK + mid2
                elif owner == "3":
                    namebowl += Bcolours.PLAY3 +  (" " * (namediff1 + 1)) + name + (" " * namediff2) + Bcolours.ENDCBLOCK + mid2
                    shipbowl += Bcolours.PLAY3 + (" " * (shipdiff1 + 1)) + numberShips + (" " * (shipdiff2)) + Bcolours.ENDCBLOCK + mid2
                    ownerbowl += Bcolours.PLAY3 + (fillChar * (diff1 + 1)) + owner + (fillChar * diff2) + Bcolours.ENDCBLOCK + mid2
                elif owner == "4":
                    namebowl += Bcolours.PLAY4 +  (" " * (namediff1 + 1)) + name + (" " * namediff2) + Bcolours.ENDCBLOCK + mid2
                    shipbowl += Bcolours.PLAY4 + (" " * (shipdiff1 + 1)) + numberShips + (" " * (shipdiff2)) + Bcolours.ENDCBLOCK + mid2
                    ownerbowl += Bcolours.PLAY4 + (fillChar * (diff1 + 1)) + owner + (fillChar * diff2) + Bcolours.ENDCBLOCK + mid2
                else:
                    namebowl += (" " * (namediff1 + 1)) + name + (" " * namediff2)+ mid2        
                    shipbowl += (" " * (shipdiff1 + 1)) + numberShips + (" " * shipdiff2) + mid2
                    ownerbowl += " " * (self.maxNameLength + 1) + mid2
                    
            print(ownerbowl)
            print(namebowl)
            print(shipbowl)
            print("+" + (self.width*topbottom))
            
    def getAllPlanets(self):
        planets = {}
        for y in range(self.height):
            for x in range(self.width):
                tmp = self.getPlanetByXY(x, y)
                if tmp:
                    planets[x,y] = tmp
        return planets
    
    def toJSON(self):
        planets = self.getAllPlanets()
        outArray = []
        for i in planets.keys():
            planet = planets[i]
            outDict = {'coords' : i, 'name': planet.name, 'owner': planet.owner, 'killp': planet.killPercentage, 'prodrate': planet.prodrate, 'ships': planet.ships}
            outArray.append(outDict)
            
        outDict2 = {'height' : self.height, 'width' : self.width, 'planets': outArray}
        
        outJSON = json.dumps(outDict2)
            
        return outJSON
        
    def fromJSON(self, jsonBoard):
        injson = json.loads(jsonBoard)
        planets = injson['planets']
        self.height = injson['height']
        self.width = injson['width']
        
        self.twodBoard = [[0 for y in range(self.height)] for x in range(self.width)]
        
        tmpNameLen = 0
        
        for i in planets: # dict of each planet
            tmpPlanet = Planet(i['name'], True)
            tmpPlanet.owner = i['owner']
            tmpPlanet.killPercentage = i['killp']
            tmpPlanet.prodrate = i['prodrate']
            tmpPlanet.ships = i['ships']
            x = i['coords'][0]
            y = i['coords'][1]
            
            self.twodBoard[x][y] = tmpPlanet
            
            if len(i['name']) > tmpNameLen:
                tmpNameLen = len(i['name'])
                
        self.maxNameLength = tmpNameLen
            
            
            
        
