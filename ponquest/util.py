import random
from bcolours import Bcolours

def killRandom():
    sigma = 0.15
    mu = 0.5
    
    randomness = random.normalvariate(sigma, mu)
        
    while randomness < 0.4 or randomness > 1:
        randomness = random.normalvariate(sigma, mu)
    return randomness

def playerText(curPlayer, endc):
    endChar = ""
    if endc:
        endChar = Bcolours.ENDC
    if curPlayer == 1:
        return str(Bcolours.PLAY1 + "Player " + str(curPlayer) + endChar)
    elif curPlayer == 2:
        return str(Bcolours.PLAY2 + "Player " + str(curPlayer) + endChar)
    elif curPlayer == 3:
        return str(Bcolours.PLAY3 + "Player " + str(curPlayer) + endChar)
    elif curPlayer == 4:
        return str(Bcolours.PLAY4 + "Player " + str(curPlayer) + endChar)
    elif curPlayer == 0:
        return str(Bcolours.BOLD + "Neutral" + endChar)
    else:
        return str(curPlayer)

def checkEqual(iterator):
    iterator = iter(iterator)
    try:
        first = next(iterator)
    except StopIteration:
        return True
    return all(first == rest for rest in iterator)
