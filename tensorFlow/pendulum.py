import gym
import tensorflow as tf
import numpy as np
import math # gives the sigmoid() function
import time

env = gym.make('Pendulum-v0')
env.reset()

total_reward = tf.placeholder(tf.float32)
H = 10 # num of hidden neurons

 # model

ob = tf.placeholder(tf.float32, shape=(3, 1))
W1 = tf.Variable(tf.ones([H, 3]))
wtimeso = tf.matmul(W1, ob)

W2 = tf.Variable(tf.ones([1, H]))
move = tf.matmul(W2, wtimeso) # this is one number in a 2d array
 # to run action, you will need to feed it -- observations (to the var 'ob')


loss = -total_reward
optimizer = tf.train.GradientDescentOptimizer(learning_rate=(0.01))
update = optimizer.minimize(loss) 

episode_reward = 0
total_episodes = 1000
episode_num = 0
batch_reward = 0 
init = tf.initialize_all_variables()

with tf.Session() as sess:
    sess.run(init)
 

    while episode_num <= total_episodes:
        
        # runs the game
        
        if episode_num > 100:
            env.render()
        
        numpy.reshape(observation, (3,1))
        action = sess.run(move, feed_dict={ob: observation})
        file_writer = tf.summary.FileWriter('.', sess.graph)

        # sigmoid func for action
        if action < -2:
            action = -2
        elif action > 2:
            action = 2
            
        observation, reward, done, info = env.step(action)
        
        episode_reward += reward
        #print episode_reward
        #time.sleep(.3)
        
        if done: # stops the game when done
            episode_num = episode_num+1
            batch_reward = batch_reward+episode_reward # collects the ten games of rewards
            
            episode_reward = 0 # resets the episode_reward after each episode
            
            if episode_num % 5 == 0: # updates vars each 10 games
                b_reward = batch_reward[0]
                print b_reward
                
                sess.run(update, feed_dict={total_reward: b_reward})
                
                batch_reward = 0 # rests the batch reward after each batch
                
