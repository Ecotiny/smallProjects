import tensorflow as tf

from tensorflow.examples.tutorials.mnist import input_data

mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)


x = tf.placeholder(tf.float32, [None, 784]) # what we choose to input into program, n_number of images 
W = tf.Variable(tf.zeros([784, 10])) # weights 784 images, 10 different possibilities for each
b = tf.Variable(tf.zeros([10])) # biases only on for each image

y_ = tf.placeholder(tf.float32, [None, 10]) # where to imput correct values

y = tf.nn.softmax(tf.matmul(x, W) + b) # entire model summed up
 # tf.matmul just used to multiply the x and W tensors. both tensors are matrices, and matmul stands for matrix multiply
 # softmax returns an index of numbers between 0 and 1 that add up to 1

cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1]))
 # (how much wrong) = -(correct answer)*log(guess) << the cross_entropy way
 # tf.reduce_sum adds the elements in the second dimension of y cos' reduction_indices is 1
 # tf.reduce_mean computes the mean over all the examples in the batch
 
train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)
 # 0.5 is the "training step" how much it changes vars each time
 
init = tf.initialize_all_variables()

sess = tf.Session()
sess.run(init)

for i in range(1000):
  batch_xs, batch_ys = mnist.train.next_batch(100)
  sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})
  # tf.summary.scalar('loss', cross_entropy, collections=None) << could try running tensorboard with this
  # passing along the vars -- we feed x and y_ into train_step. we do this because we need to feed cross_entropy
  # cross_entropy takes in the y_ (the correct data) and the the y in cross_entropy needs feeding so we 
  # feed it x way back from train_step. pretty neato huh

correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))
 # tf.argmax gives the index of the highest entry in a tensor along some axis
 # gives a list of booleans 
 
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
 # [true, true, false, true] would become [1, 1, 0, 1] then 0.75 
 
print(sess.run(accuracy, feed_dict={x: mnist.test.images, y_: mnist.test.labels}))
 # fed the y_ (correct stuff) through to correct_prediction through accuracy
 # the x is fed to the model (y) through correct_prediction which is fed through accuracy
 
