import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

# n_bandit reinforcement learning exercise

bandit_machines = [0.25, 0.5, 0.1]
num_bandits = len(bandit_machines)
# environment

def pullbandit ( bandit_value ):
    # get random num between 0 and 1 and if that is less than the 
    # corresponding bandit machine, give 1 else give -1
    b = np.random.rand(1)
    if bandit_value < b:
        reward = -1.0
    else:
        reward = 1.0
        
    return [reward]

# agent

weights = tf.Variable(tf.ones(num_bandits)) #tf.ones(num_bandits)
which_arm = tf.argmax(weights, 0) # the 0 is the axis of the tensor to look at

weight_index = tf.placeholder(dtype = tf.int32) # index of chosen weight
chosen_weight = tf.slice(weights, weight_index, [1]) # the chosen weight
reward_holder = tf.placeholder(dtype = tf.float32) # either 1 or -1
loss = -(tf.log(chosen_weight)*reward_holder) # loss = -log(policy)*advantage
#loss = -(chosen_weight*reward_holder) # loss = -log(policy)*advantage

optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.001)
update = optimizer.minimize(loss)

# loop

init = tf.initialize_all_variables()

with tf.Session() as sess:
    
    sess.run(init)
    i = 0
    x = []
    bandit_one = []
    bandit_two = []
    bandit_three = []
    while i < 10000:
        
        x.append(i)
        
        # here be system of having a chance of randomly chosing a bandit
        e = 0.1
        if np.random.rand(1) < e:
            # choose a random bandit 
            action = np.random.randint(3) # it has to either return 0 or 1 or 2
        else:
            # choose the highest weight bandit
            action = sess.run(which_arm)
            
        reward = pullbandit(bandit_machines[action])
        
        _, resp, ww, nice_weights = sess.run([update, chosen_weight, reward_holder, weights], feed_dict = {weight_index:[action], reward_holder:[reward]})
        
        bandit_one.append(nice_weights[0])
        bandit_two.append(nice_weights[1])
        bandit_three.append(nice_weights[2])
        
        i+=1
        if i % 50 == 0:
            print sess.run(weights)
    print bandit_machines
    print 'the computer thinks that bandit number {} is the the best bandit'.format(np.argmax(nice_weights) + 1)
    print 'number {} is the best bandit'.format(np.argmax(bandit_machines, 0)+1)
    
one, = plt.plot(x, bandit_one, label="Bandit 1")
two, = plt.plot(x, bandit_two, label="Bandit 2")
three, = plt.plot(x, bandit_three, label="Bandit 3")
plt.legend(handles=[one,two,three])
plt.show()
