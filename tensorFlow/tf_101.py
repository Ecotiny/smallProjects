import tensorflow as tf

node1 = tf.constant(3.0, tf.float32)
node2 = tf.constant(4.0) # also tf.float32 implicitly
node4 = tf.constant(7.0) # also tf.float32 implicitly
node3 = tf.add(node1, node2)
node5= tf.add(node3, node4)

print(node1, node2, node3)

sess = tf.Session()
file_writer = tf.summary.FileWriter('.', sess.graph)

print(sess.run([node1, node2, node3, node5]))
