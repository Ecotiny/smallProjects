import gym
import tensorflow as tf
import numpy as np
import math # gives the sigmoid() function
import time

env = gym.make('CartPole-v0')
env.reset()

total_reward = tf.placeholder(tf.float32)
H = 10 # num of hidden neurons

 # model

ob = tf.placeholder(tf.float32, shape=(4, 1))
W1 = tf.Variable(tf.ones([H, 4]))
wtimeso = tf.matmul(W1, ob)

W2 = tf.Variable(tf.ones([1, H]))
move = tf.matmul(W2, wtimeso) # this is one number in a 2d array
 # to run action, you will need to feed it -- observations (to the var 'ob')


loss = tf.multiply(total_reward, -1)
optimizer = tf.train.GradientDescentOptimizer(learning_rate=(0.01))
update = optimizer.minimize(loss) 

episode_reward = 0
total_episodes = 1000
episode_num = 0
batch_reward = 0 
init = tf.initialize_all_variables()

with tf.Session() as sess:
    
    sess.run(init)
    observation = env.reset() # initial observation
    
    while episode_num <= total_episodes: # run each episode
        
        action = sess.run(move, {ob: observation})
        
        if action > 0.5: 
            action = 1 
        else:
            action = 0
        observation, reward, done, info = env.step(action)
        episode_reward += reward
        
        if done: # after each episode
            
            observation = env.reset() # initial observation for next episode
            batch_reward += episode_reward
            episode_reward = 0
            
            if episode_num % 5 == 0: #after each batch
                
                sess.run(update, {})
                batch_reward = 0
        
