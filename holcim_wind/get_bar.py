from PIL import Image
import numpy as np
from os import listdir

# min and max in terms of graph in image
min_y = 49
max_y = 577
max_gust_rgb = (1,1,1) # the black colour used in the gust bar
avg_rgb = (195,18,10) # the red colour used in the avg bar

def get_colour_diff(rgb1, rgb2):
    r1, g1, b1 = rgb1
    r2, g2, b2 = rgb2
    d = np.sqrt((r2-r1)**2+(g2-g1)**2+(b2-b1)**2)
    return d


def y_pos_to_knots(y_pos):

    max_knots = 70
    min_knots = 0
    
    multiplier = max_knots / float(max_y - min_y)
    
    y_pos -= min_y # relative to top of graph
    speed = y_pos * multiplier # inverted speed
    knots = max_knots - speed
    
    return knots
    
### ACTUAL FUNC

def scan_x(x_val, image, colour, test_others=True): # this will scan an x val and give a bearing
    
    # get all colour differences along that line
    colour_diffs = []
    diff_threshold = 20 # this will determine if I consider a pixel black enough

    for y in range(min_y, max_y):
        pixel = image.getpixel((x_val, y))
        diff = get_colour_diff(pixel, colour)
        
        if diff < diff_threshold:
            colour_diffs.append(y)
    
    override_max_gust = False # in case there is a max gust found
    
    if len(colour_diffs) == 0:
        # r/programminghorror coming right up (worst way to do recursion)
        
        if test_others: # make sure we control extent of 
        
            test_max_gust = scan_x(x_val-1, image, False)
            
            if not test_max_gust:
                #print("None in that x val, trying next x val")
                test_max_gust = scan_x(x_val+1, image, False)
                if not test_max_gust:
                    
                    test_max_gust = scan_x(x_val-2, image, False)
            
                    if not test_max_gust:
                        test_max_gust = scan_x(x_val+2, image, False)
                        if not test_max_gust:
                            print("ah bugger")
                            print("X VAL OF FALSE IS... {}\n!!!!!!!!!!!!!!!!!!!".format(x_val))
                            return False
                        else:
                            #print("AH ha found it\n============")
                            override_max_gust = test_max_gust
                    else:
                        override_max_gust = test_max_gust
                else:
                    #print("AH ha found it\n============")
                    override_max_gust = test_max_gust
            else:
                #print("AH ha found it\n============")
                override_max_gust = test_max_gust
        else:
            return False
        
    
    if not override_max_gust:
        max_gust_y = 0
        
        # check there are 2 consecutive black pixels found (otherwise might get values from grid lines)
        for ind in range(len(colour_diffs)):
            if ind != len(colour_diffs) - 1: # if not the last index in the array
                if colour_diffs[ind+1] - colour_diffs[ind] == 1: # if they are consecutive, this one will be the max gust
                    max_gust_y = colour_diffs[ind]
        
        if max_gust_y != 0:
            max_gust = round(y_pos_to_knots(max_gust_y), 3)
            
            return max_gust
        else:
            return False
    else:
        return override_max_gust
    
recent_x = 892 # INACCURATE recent always lie on this x position
last_x = 80 # INACCURATE oldest at this x position

entries_in_graph = 144

# Find where the bar lies by scanning the bottom line of pixels on the graph
def find_bars(image, colour, y_val):
    for x in range(77, 898): #77 is left side of graph, 898 is right side of graph
        pixel = image.getpixel((x, y_val))
        

def scan_directory(directory):
    
    # make sure there is a trailing slash after directory
    if directory[:-1] != "/":
        directory += "/"
    
    max_gusts = []

    filenames = listdir(directory)

    for fn in filenames:

        img = Image.open(directory+fn).convert("RGB")
        
        for x in np.linspace(last_x, recent_x, num=entries_in_graph):
            x = round(x, 0)
            max_gusts.append(scan_x(x, img))
            
    return max_gusts
