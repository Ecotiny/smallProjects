import get_bar_w_detection
import get_bearing
import matplotlib.pyplot as plt

avg_rgb = (195,18,10) # the red colour used in the avg bar
max_gust_rgb = (1,1,1) # the black colour used in the gust bar

directory = "images/"

bearings = get_bearing.scan_directory(directory)
avgs = get_bar_w_detection.scan_dir_colour(directory, avg_rgb, 40)
max_gusts = get_bar_w_detection.scan_dir_colour(directory, max_gust_rgb, 10)


plt.plot(avgs)
plt.plot(max_gusts)
plt.show();
