import requests
from datetime import datetime, timedelta
from PIL import Image
from io import BytesIO

now = datetime.now()


time_interval = timedelta(hours=24)
print(time_interval)


def to_url(dt):
    # every 10 minutes so round to nearest 10 minutes ONLY NEED EVERY 24 HOURS
    minutes = dt.minute - (dt.minute % 10)
    hour = dt.hour
    day = dt.day
    month = dt.month
    year = dt.year

    dt_rounded = datetime(year,month,day,hour,minutes)

    url_date_str = dt_rounded.strftime("%d-%m-%Y_%I-%M") + dt_rounded.strftime("%p").lower() # use normal date then lowercase am/pm

    return "https://www.portotago.co.nz/assets/Tide-and-Wind/Wind-Holcim-Silo/HolcimSilo_{}.jpg".format(url_date_str)
    # https://www.portotago.co.nz/assets/Tide-and-Wind/Wind-Holcim-Silo/HolcimSilo_27-11-2018_07-00am.jpg

fourohfour = False

while not fourohfour:
    
    url = to_url(now)
    r = requests.get(url)
    formatted_date = now.strftime("%Y-%m-%d_%H-%M")
    if r.status_code == 404:
        fourohfour = True
        print("404 at: {}".format(formatted_date))
        break
    i = Image.open(BytesIO(r.content))
    i.save("images/{}.jpg".format(formatted_date));
    now = now - time_interval
