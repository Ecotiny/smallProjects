from PIL import Image
import numpy as np
from os import listdir

bearing_rgb = (241,139,41) # the orange colour used in the bearing shape

def get_colour_diff(rgb1, rgb2):
    r1, g1, b1 = rgb1
    r2, g2, b2 = rgb2
    d = np.sqrt((r2-r1)**2+(g2-g1)**2+(b2-b1)**2)
    return d


def y_pos_to_bearing(y_pos): # degrees esst of north
    min_y = 49
    max_y = 577
    max_angle = 360
    min_angle = 0
    
    multiplier = max_angle / float(max_y - min_y)
    
    y_pos -= min_y # relative to top of graph
    angle = y_pos * multiplier # inverted bearing
    bearing = max_angle - angle
    
    return bearing
    
### ACTUAL FUNC

def scan_x(x_val, image): # this will scan an x val and give a bearing
    
    # get all colour differences along that line
    colour_diffs = []
    diff_threshold = 39 # this will determine if I consider a pixel orange enough

    for y in range(image.height):
        pixel = image.getpixel((x_val, y))
        diff = get_colour_diff(pixel, bearing_rgb)
        
        if diff < diff_threshold:
            colour_diffs.append(y)
    
    if len(colour_diffs) == 0:
        print("AHH FREK no Oranges FOundasaiuolhasdflkhjasdflkjhasdf god it is too late and I am too tired\nX: {}".format(x_val))
        return False
    
    mean_y = int(np.mean(colour_diffs)) # get mean
    print("y: {}".format(mean_y))
    bearing = round(y_pos_to_bearing(mean_y), 3)
    
    return bearing
    
recent_x = 891 # recent wind bearings always lie on this x position
last_x = 79 # oldest wind bearing at this x position

entries_in_graph = 144

def scan_directory(directory):
    
    # make sure there is a trailing slash after directory
    if directory[:-1] != "/":
        directory += "/"
    
    bearings = []

    filenames = listdir(directory)

    for fn in filenames:
        img = Image.open(directory+fn).convert("RGB")
        for x in np.linspace(last_x, recent_x, num=entries_in_graph):
            x = round(x, 0)
            print(x)
            bearings.append(scan_x(x, img))
            
    return bearings
