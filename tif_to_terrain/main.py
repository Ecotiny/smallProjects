import cv2
import numpy as np
import argparse
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

refPt = []

def capture_clicks(event, x, y, flags, param):

    global refPt

    if event == cv2.EVENT_LBUTTONDOWN:
        refPt = [(x, y)]
    elif event == cv2.EVENT_LBUTTONUP:
        refPt.append((x, y))

def crop(img):
    print("Getting coordinates")

    cv2.namedWindow("image")
    cv2.setMouseCallback("image", capture_clicks)
    while len(refPt) < 2:
        cv2.imshow("image", img)
        key = cv2.waitKey(1) & 0xFF

    cv2.destroyAllWindows()
    print("I have coordinates")
    print(refPt)
    roi = img[refPt[0][1]:refPt[1][1], refPt[0][0]:refPt[1][0]]
    return roi

if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "im",
        help="Image path")

    parser.add_argument(
        "--max",
        type=int,
        default=50,
        help="Maximum height (mm)")

    parser.add_argument(
        "--min",
        type=int,
        default=5,
        help="Minimum height of base (mm)")

    args = parser.parse_args()
    img = cv2.imread(args.im, -1)

    #img = crop(img)
    img = img[6144:6144+2016, 3456:3456+3184]
    cv2.imwrite("output.png", img)
