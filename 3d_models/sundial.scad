dial_diameter = 200;
dial_radius = dial_diameter / 2;
latitude = 45.856766408;
gnomon_length = 50;
gnomon_diameter = 8;
support_length = dial_radius-(gnomon_diameter/2) * tan(latitude);
thickness = 10;
use <quickthread.scad>;

// dial plate
module dial_ring() {
    difference() {
        cylinder(d=dial_diameter, h=thickness, center=true);
        cylinder(d=dial_diameter - (thickness*2), h=thickness+2, center=true);
    }
}

// time marker
module marker(thick=false) {
    if (thick==true) {
        width = 5;
        translate([0,0,3/2])cube([thickness, 5, 3], center=true);
    } else {
        width = 3;
        translate([0,0,3/2])cube([thickness, 3, 3], center=true);
    }
    
}

//cross supports for gnomon
module dial_cross() {
    cube([dial_diameter - thickness,thickness,thickness], center=true);
    rotate(90)cube([dial_diameter - thickness,thickness,thickness], center=true);
}

// whole assembly
module dial_face() {
    difference() {
        union() {
            dial_ring();
            dial_cross();
            for (i=[0:240]) {
                if (i % 15 == 0) {
                    rotate(i + 60)translate([dial_radius-(thickness/2),0,thickness/2])marker(false);
                    rotate(i + 60)translate([dial_radius-(thickness/2),0,-thickness/2 -3])marker(false);
                }    
            }
        }
        translate([0,0,-thickness])isoThread(d=gnomon_diameter+0.5, h=thickness*2, $fn=24);
    }
    
}

//gnomon
module gnomon() {
    $fn = 30;
    difference() {
        union() {
            cylinder(d=gnomon_diameter,h=support_length);
            translate([0,0,support_length])isoThread(d=gnomon_diameter, h=thickness,$fn=24);
            translate([0,0,support_length+thickness])cylinder(d=gnomon_diameter-2,h=gnomon_length);
        }
        // ground plane
        rotate([0,90-latitude,0])translate([0,0,-50])cube([1000,1000,100], center=true);
        
    }
}

module sundial() {
    translate([0,0,sqrt(2*pow(dial_radius,2))/2])union() {
        rotate([0,-(90-latitude),0])translate([0,0,-support_length-thickness/2])gnomon();
        rotate([0,-latitude,0])dial_face();
    }
}

sundial();
// 