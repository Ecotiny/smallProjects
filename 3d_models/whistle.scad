// whistle design
$fn = 20;


module main_body() {
    difference() {
        cylinder(d=outside_diam, h=outside_length);
        translate([0,0,-1])cylinder(d=inside_diam, h=inside_length+1); // +1 to account for translation
    }
}

module whistle_cut() {
    difference() {
        cube([outside_diam + 1, outside_diam + 1, 50]);
        translate([0,-0.5,0])rotate([0,-cut_angle,0])cube([100, 100, 100]);

    }
}

module taper() {
    echo(atan2(inside_diam, taper_length) - 5);
    rotate()difference() {
        cylinder(d=inside_diam, h=taper_length);
        rotate([0, atan2(inside_diam, taper_length) - 3,0])translate([-inside_diam/2,-5,-5])cube([10,10,taper_length+10]); // -5 to angle to give space for air
    }
}

module whistle() {
    taper();
    difference() {
        main_body();
        translate([0,-(outside_diam+1)/2, taper_length + cut_offset])whistle_cut();
    }
}

speed_of_sound = 343;
desi_freq = 13000;

chamber_length = ((speed_of_sound/desi_freq)*250); // /4 (magic number) then * 1000 (convert to mm from m) would do the same thing

echo(chamber_length);

outside_diam = 9;
inside_diam = 6;
cut_angle = 40;
taper_length = 10;
outside_length = chamber_length+taper_length+3;
inside_length = chamber_length+taper_length;
cut_offset = -0.5;

whistle();
// test hole on other side
//difference() {
//    whistle();
//    translate([-inside_diam/2+1,0,taper_length])rotate([90,0,-90])cylinder(d=1.5, h=100);
//}