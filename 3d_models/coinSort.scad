coinOne = 22;
coinTwo = 23.5;
coinThree = 24;
coinFour = 26.5;
coinFive = 29;
total = coinOne+coinTwo+coinThree+coinFour+coinFive;
translate([-5,0,0])intersection() {
    difference() {
        translate([0,0,20])rotate([25,15,0])difference() {
            hull() {
                cube([total+20, 15, coinFive+10]);
                #translate([0,-15,-(coinFive/2+10.5)])rotate([-25,-10,0])cube([1,40,1]);
            }
            translate([-2.5,5,5])cube([total+25,7,coinFive+10]);
            translate([1,0,0])union() {
                translate([total-coinFive-coinFour-coinThree-coinTwo-9,7.5,coinOne/2+5])rotate([90,0,0])cylinder(d=coinOne, h=30);
                translate([total-coinFive-coinFour-coinThree-6,7.5,coinTwo/2+5])rotate([90,0,0])cylinder(d=coinTwo, h=30);
                translate([total-coinFive-coinFour-3,7.5,coinThree/2+5])rotate([90,0,0])cylinder(d=coinThree, h=30);
                translate([total-coinFive,7.5,coinFour/2+5])rotate([90,0,0])cylinder(d=coinFour, h=30);
                translate([total+3,7.5,coinFive/2+5])rotate([90,0,0])cylinder(d=coinFive, h=15);
            }
        }
        translate([-250,-250,-500])rotate([0,0,0])cube([500,500,500]);
    }
     translate([6.5,0,0])cube([100,200,200], center=true);
}
//translate([0,0,0])intersection() {
//    difference() {
//        translate([0,0,10])rotate([25,5,0])difference() {
//            hull() {
//                cube([total+20, 15, coinFive+10]);
//                #translate([0,-15,-(coinFive/2+10.5)])rotate([-25,-10,0])cube([1,40,1]);
//            }
//            translate([-2.5,5,5])cube([total+25,7,coinFive+10]);
//            translate([1,0,0])union() {
//                translate([total-coinFive-coinFour-coinThree-coinTwo-9,7.5,coinOne/2+5])rotate([90,0,0])cylinder(d=coinOne, h=30);
//                translate([total-coinFive-coinFour-coinThree-6,7.5,coinTwo/2+5])rotate([90,0,0])cylinder(d=coinTwo, h=30);
//                translate([total-coinFive-coinFour-3,7.5,coinThree/2+5])rotate([90,0,0])cylinder(d=coinThree, h=30);
//                translate([total-coinFive,7.5,coinFour/2+5])rotate([90,0,0])cylinder(d=coinFour, h=30);
//                translate([total+3,7.5,coinFive/2+5])rotate([90,0,0])cylinder(d=coinFive, h=15);
//            }
//        }
//        translate([-250,-250,-500])rotate([0,0,0])cube([500,500,500]);
//    }
//     #translate([155,0,0])cube([200,200,200], center=true);
//}