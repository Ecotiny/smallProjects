rotate([180,0,0])difference() {
    hull() {
        cube([30,10,30]);
        translate([50/2,0,50/2])translate([-40,0,25])cube([50,100,25]);
    }
    hull() {
        translate([1.5,1.5,1.5])cube([27,7,15]);
        translate([47/2,0,47/2])translate([-37,1.5,30])cube([47,97,23]);
    }
     translate([1.5,1.5,-10])cube([27,7,100]);
}