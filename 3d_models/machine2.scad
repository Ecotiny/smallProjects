$fn=50;
rotate([0,90,0])scale([10,10,10])difference() {
    union() {
        difference() {
            sphere(r=5);
            sphere(r=3);
           
        }
         for (i = [0:5:720]) {
               rotate([0,0,i])translate([pow(i,0.9)/150,0,i/650-6])rotate([9,0,0])cube([1,0.5,0.5]);
            }
        difference() {
            translate([0,0,-3])sphere(r=3);
            sphere(r=3);
        }
        translate([0,0,5])cylinder(r=0.5,h=6);
        translate([-1.25,-2.5,2.5])rotate([45,0,0])cube([2.5,1,2.5]);
      rotate([-90,0,0])translate([-1.25,-2.5,2.5])rotate([45,0,0])cube([2.5,1,2.5]);

   }
    translate([0,-25,-30])cube([50,50,600]);
}