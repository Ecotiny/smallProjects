module drill() {
    hull() {
        cylinder(r1=1,r2=15,h=35);
        translate([0,0,35])cylinder(r=8,h=13);
    }
    for (i = [0:5:1080]) {
        rotate([0,0,i])translate([i/75,0,i/30])rotate([9,0,0])cube([3,3,0.5]);
    }
}
module driver() {
        translate([0,0,35])cylinder(r=8,h=20);
}
scale([1.325,1.325,1.325])rotate([0,90,0])difference() {
    union() {
        drill();
        driver();
        translate([0,0,55])cylinder(r=1,h=20);
    }
    translate([0,0,28])cylinder(r=6,h=25);
    translate([0,-25,0])cube([550,50,100]);
}

