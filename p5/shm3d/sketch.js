

var angle = 0;
var boundXY = 200;
var boundZ = 1000;
var boxX = 32;
var boxY = 32;
var boxWidth = 5;
var maxDist;

function setup() {
    createCanvas(800,800, WEBGL);
    ortho(-boundXY, boundXY, -boundXY, boundXY, 0, boundZ);
    maxDist = sqrt(2*((boxY*boxWidth)**2))
}

function draw() {
    background(25);

    angle -= PI/32;
    normalMaterial();
    for (var z = -boxY/2;z < boxY/2;z++) {
        for (var x = -boxX/2;x < boxX/2;x++) {
            push();
            rotateX(-PI/4);
            rotateY(atan(1/sqrt(1.1)));
            var x1 = x * boxWidth;
            var z1 = z * boxWidth;
            translate(x1,0,z1);
            var d = map(dist(0,0,0,x1,0,z1), 0, maxDist, -PI, PI);
            var h = floor(map(sin(angle + d), -1, 1, 10, 100));

            box(boxWidth,h,boxWidth);
            pop();
        }
    }
}
