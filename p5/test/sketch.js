function setup() {
    createCanvas(800,800);
}

var prevX = 0;
var prevY = 0;
var gravity = 1;
var v = 0;
var diam = 20;
var drag = 0.5;
var counter = 0;

function draw() {
    background(0);
    stroke(255,0,255);
    fill(255,0,255);
    if (mouseIsPressed) {
        strokeWeight(5);
        line(mouseX, mouseY, prevX, prevY)
        ellipse(mouseX,mouseY,diam,diam);
        prevX = mouseX;
        prevY = mouseY;
        v = 0;
    } else {
        v += gravity;
        let newX = prevX;
        let newY = prevY + v;
        if (newY <= height - diam/2) {
            strokeWeight(5);
            line(newX, newY, prevX, prevY);
            ellipse(newX, newY,diam,diam);
            prevX = newX;
            prevY = newY;
        } else {
            v = -v * (1-drag);
            v += gravity;
            let newX = prevX;
            let newY = prevY + v;
            strokeWeight(5);
            line(newX, newY, prevX, prevY);
            ellipse(newX, newY, diam, diam);
            prevX = newX;
            prevY = newY;
        }
    }

}
