#include <iostream>
#include <vector>
#include <stdint.h>

std::vector<uint64_t> primes;

bool is_prime(uint64_t number) {
    	for (auto prime : primes) {
        	if (number % prime == 0) {
			return false;
		}
	}
	primes.push_back(number);
	return true;
}

int main() {
	uint64_t number = 2;
	int n = 0;
	int max_n;
	std::cin >> max_n;
	while (n < max_n) {
		if (is_prime(number)) {
			n++;
			std::cout << number << "\n";
		}
		number++;
	}
}
