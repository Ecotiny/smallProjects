#include <iostream>
#include <math.h>
#include <list> 
#include <algorithm>

int main() {
	std::list <int> comparators;
	std::list <int> found_primes;

	// starting at 2, find the square, add it to comparators
	found_primes.push_back(2);
	comparators.push_back(2*2);

	long long current_number = 2;
	int num_of_primes = 1000000;
	while (found_primes.size() < num_of_primes) {
		// increase current number, see if it's in comparators
		// if it isn't, add it to found primes and then add the squre to the comparators
		// if it is, increase it by the number stored at the same index of the found primes list
		current_number++;
    		std::list<int>::iterator findIter = std::find(comparators.begin(),
				comparators.end(),
				current_number);
		if (findIter == comparators.end()) {
			found_primes.push_back(current_number);
			comparators.push_back(current_number*current_number);

    			//for (auto v : found_primes) {
        		//	std::cout << v << " ";
			//}
			//std::cout << "\n";
			//for (auto v : comparators) {
			//	std::cout << v << " ";
			//}
			//std::cout << "\n";
		} else {
			while (findIter != comparators.end()) {
				int index = std::distance(comparators.begin(), findIter);

				// Create iterator pointing to first element
				std::list<int>::iterator newIter = found_primes.begin();

				// Advance the iterator by index positions,
				std::advance(newIter, index);
			
				// newIter now points at the element which is the prime number that the comparator is related to
				int prime = *newIter;
				*findIter += prime;

				findIter = std::find(findIter,
						comparators.end(),
						current_number);
			}
		}	
	}
	std::cout << "FOUND " << current_number << "\n";
}

