# hack font is 2:1
# a4 paper is 13:9 (ish)
# assume font size pt 2 (0.7mm)
# going onto a4 paper with a margin of 2cm (20mm) making the dimensions 210x297 become 170x257 (20mm on either side)

from PIL import Image

sample_image = "mona_lisa_cropped.jpg"  # img cropped to 1:1.4142
image = Image.open(sample_image)
pixels = image.load()

sample_text = "input.txt"
text_file = open(sample_text, "r")
text = text_file.read()
text_file.close()

# divide image into little pieces each of which each have a ratio of 2:1
# each piece should have a height of 0.7mm
# assume image going to be expanded to 170x257mm
libreoffice_offset_y = 0.2   # because libreoffice must have a 0.05cm line spacing (0.5mm)
libreoffice_offset_x = 0.09
num_height_divisions = int(round(400/(0.7 + libreoffice_offset_y), 0))
# width of each char must be 0.35mm then
num_width_divisions = int(round(257/(0.35 + libreoffice_offset_x), 0))

# have output list with correct height and width
output_list = [[""]*num_height_divisions for _ in range(num_width_divisions)]

# find the number of pixels each division represents on the image
pixelH = int(round(image.height / num_height_divisions, 0))
pixelW = int(round(image.width / num_width_divisions, 0))

spacer_counter = 0  # for when we need to skip some characters for whitespace
text_idx_counter = 0  # to get the correct character from the sample text

for y in range(num_height_divisions):
    imgY = y * pixelH
    for x in range(num_width_divisions):
        imgX = x * pixelW
        if spacer_counter > 0:
            if x == 0:
                spacer_counter = 0
            spacer_counter -= 1
            output_list[x][y] = " "
            continue
        # run through section for this character
        section = []  # going to hold brightness of each pixel in section
        for x1 in range(pixelW):
            for y1 in range(pixelH):
                if (x1 + imgX) >= image.width:
                    pixelX = image.width - 1
                else:
                    pixelX = x1 + imgX

                if (y1 + imgY) >= image.height:
                    pixelY = image.height - 1
                else:
                    pixelY = y1 + imgY

                pixel = pixels[pixelX, pixelY]

                brightness = sum(pixel)
                section.append(brightness)

        # find avg for section
        brightness_total = sum(section) / len(section)
        # this will be in range (0,765) 765 is 255*3
        # divide this down into 0,max_spaces
        max_spaces = 20
        brightness_total = int(round(brightness_total * (max_spaces/765), 0))  # round it too
        spacer_counter = brightness_total
        if text_idx_counter >= len(text):
            text_idx_counter = 0
            print("finished script")
        output_list[x][y] = text[text_idx_counter]
        #print("adding {}".format(text[text_idx_counter]))
        text_idx_counter += 1
        #print(x, y)
        #print(output_list)

output_string = ""
for y in range(len(output_list[0])):
    for x in range(len(output_list)):
        output_string += output_list[x][y]
    output_string += "\n"


# output txt file
output_fn = "output.txt"
of = open(output_fn, "w")
#print("output: {}".format(output_string))
of.write(output_string)
of.close()
