import tensorflow as tf
import numpy as np

csvData = open('Test.csv', 'r')
lines = csvData.read().split("\n")
properCSVdata = []

for line in lines:
    if line != "": # add other needed checks to skip titles
        cols = line.split(",")
        properCSVdata.append(cols)
            
def get_data(y, x): # 0 is sell, 1 is buy (on the x axis)
    return float(properCSVdata[y][x])

def global_timestep(fed_timestep, day):
    return (((day+1)*timesteps_in_a_day)+fed_timestep)

def get_inputs(fed_timestep, day): 
    glob_time = global_timestep(fed_timestep, day)
    inputs_matrix = []
    for i in range(60):# sell data
        inputs_matrix.append(get_data(glob_time-(i*-48) ,0))
    for i in range(60):# buy data
        inputs_matrix.append(get_data(glob_time-(i*-48) ,1))
    
    inputs_matrix = np.reshape(inputs_matrix, (1, num_of_inputs)) # making sure inputs_matrix is the right shape
    return inputs_matrix

def simulator(action, timestep, day): # if action = 1, sell dsh , if action -1, buy dsh
    glob_time = global_timestep(timestep, day)
    sell_price = get_data(glob_time, 0)
    buy_price = get_data(glob_time, 1)
    fee = 0.02 # in decimal value 
    BTC_bal = 1
    DSH_bal = 0
    if action == 1:
        if DSH_bal > 0:
            BTC_bal = (DSH_bal/sell_price)*(1-fee)
            DSH_bal = 0
    elif action == -1:
        if BTC_bal > 0:
            DSH_bal = (BTC_bal*buy_price)*(1-fee)
            BTC_bal = 0
        
    if DSH_bal > 0:
        cur_holdings = 1
    else:
        cur_holdings = 0
        
    return cur_holdings # cur_holdings shows what is being held for that timestep
    # 1 is holding dsh, 0 is btc

def forecast(timestep, day): # for buying DSH -- CERTIFIED WORKING --
    glob_time = global_timestep(timestep, day)
    length_forecast = 60 # minutes
    timesteps_in_forecast = length_forecast*2
    forecast_list = []
    
    for i in range(1, timesteps_in_forecast+1): # gets buy price
        forecast_list.append(get_data(glob_time+i, 1))
    forecast_sum = sum(forecast_list)
    forecast = forecast_sum/len(forecast_list)
    return forecast

# hyperparameters

num_of_inputs = 120
layer_height = 200

inputs = tf.placeholder(tf.float32, [1, num_of_inputs])
w1 = tf.Variable(tf.random_normal([num_of_inputs, layer_height]))
w2 = tf.Variable(tf.random_normal([layer_height, 1]))

layer1 = tf.nn.relu(tf.matmul(inputs, w1)) # should be 1(y) by layer_height(x)
output_layer = tf.matmul(layer1, w2) # should be 1 * 1
choice = tf.nn.sigmoid(output_layer) # to run choice, give it 'inputs'

cur_buy = tf.placeholder(tf.float32, None)
fore = tf.placeholder(tf.float32, None)
# for i'th timestep in day:
# loss = (model in terms of dsh)/forecast()
# basically if the model determines an action that will make use of a high forecast 
# then loss should be low. eg.
# forecast is 0.06 for sell price: cur price = 0.05 for sell price (of dsh) and we have dsh
# and the model shows up with a 0.8 (a buy action) that is a good thing because we only want to 
# sell when the forecast is roughly the same as the cur price. (we don't want to sell yet)
# if the model turns up with a 0.2 (to sell) that is bad cos there is greater earning potential in 
# future
# to make loss use -- choice(model, need inputs for this), forecast, and what currency we held at the time
advantage = (fore-cur_buy)*100000 # high for up on graph in future
# when this advantage is high, we want the choice to be 0
# when this advantage is low, we want the choice to be 1
# choice is 1 to sell dsh, 0 to buy dsh
loss = tf.multiply(advantage, ((choice-0.5)*2))

optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.1)
backprop = optimizer.minimize(loss)

init = tf.initialize_all_variables()

days_to_go = 30 
days_done = 0


timesteps_in_a_day = (60*60*24)/30 # 60 sec*60 min*24hr/ one timestep every 30 sec
timestep = 0

with tf.Session() as sess:
    sess.run(init)
   
    while days_done <= days_to_go:
    
        hold_list = []
        buys = 0
        sells = 0
        holds = 0
        losses,forecast_list = [],[]
        
        while timestep <= timesteps_in_a_day:
        
            action_placeholder = sess.run(choice,feed_dict={inputs: get_inputs(timestep, days_done)})
            
            if action_placeholder > 0.8:# if action = 1, sell dsh , if action -1, buy dsh
                action = 1
                sells += 1
            elif action_placeholder < 0.2:
                action = -1
                buys += 1
            else:
                action = 0
                holds += 1
            
            holding = simulator(action, timestep, days_done)
            hold_list.append(holding)
                
            
            timestep += 1
            
            sess.run(backprop, feed_dict={fore:forecast(timestep, days_done), inputs:get_inputs(timestep, days_done), cur_buy:get_data(global_timestep(timestep, days_done), 1)})
            #print "what?", timestep
            losses.append(sess.run(loss, feed_dict={fore:forecast(timestep, days_done), inputs:get_inputs(timestep, days_done), cur_buy:get_data(global_timestep(timestep, days_done), 1)}))
            forecast_list.append(forecast(timestep, days_done)-get_data(global_timestep(timestep, days_done), 1))
        timestep = 0
        print "buys: ", buys, "| sells: ", sells, "| holds: ", holds, "| average loss: ", sum(losses)/2880
        print sum(forecast_list)/timesteps_in_a_day, "<-- average daily forecast"
        print
        days_done +=1
