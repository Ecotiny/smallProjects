import tensorflow as tf
import numpy as np
# make a simulated reinforcement learning thing test loss funcs
data = [0.65,0.77,0.66,0.59,0.54,0.70,0.85,0.96,0.92,0.83]
past_data = [0.54,0.60,0.57,0.67,0.81,0.76,0.74,0.72,0.74,0.70]
combinded_data = [0.54,0.60,0.57,0.67,0.81,0.76,0.74,0.72,0.74,0.70,0.65,0.77,0.66,0.59,0.54,0.70,0.85,0.96,0.92,0.83]

#for i in combinded_data:
    #print(" "*int((i*100)/2) + ":")
    
def simulation(action, time): # certified working
    if action == 1 and len(gotten_numbers) < 3:
        got_num = data[time-1]
    else:
        got_num = 0
        
    return got_num

def get_inputs(timestep): # certified working
    global_timestep = timestep + 10
    the_stuff = []
    for i in range(global_timestep-5, global_timestep):
        the_stuff.append(combinded_data[i])
    np.reshape(the_stuff, (1, 5))    
    
    return the_stuff # 5 nums, including current one

def get_result(gotten_numbers, index): # certified working
    random_list = (np.random.rand(3)+4.5)/10
    joining = random_list*gotten_numbers*2
    
    return joining[index] # so to maximise score, it should chose a high 1 and 2, then a low 3

# hyperparameters

inputs = tf.placeholder(tf.float32, [1, 5])#inputs [1, 5]
W1 = tf.Variable(tf.random_normal([5, 30]))#W1 [5, 30]
L1 = tf.nn.relu(tf.matmul(inputs, W1))#L1 [1, 30] relu
W2 = tf.Variable(tf.random_normal([30, 30]))#W2 [30, 30]
L2 = tf.nn.relu(tf.matmul(L1, W2))#L2 [1, 30] relu
W3 = tf.Variable(tf.random_normal([30, 1]))#W3 [30, 1] 
output = tf.nn.sigmoid(tf.matmul(L2, W3))#out [1, 1] sigmoid

thing_1 = tf.placeholder(tf.float32, [1])
thing_2 = tf.placeholder(tf.float32, [1])
thing_3 = tf.placeholder(tf.float32, [1])
gotten_num = tf.placeholder(tf.float32, [1, None])

loss = tf.divide(1, tf.divide(tf.multiply(thing_1, thing_2), thing_3))

optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.01)#optimizer
backprop = optimizer.minimize(loss) #backprop = ...


cur_batch_num = 1
tot_batch_num = 10

init = tf.initialize_all_variables()
print(tf.trainable_variables())

with tf.Session() as sess:
    sess.run(init)
    while cur_batch_num <= tot_batch_num:
        cur_timestep = 1
        
        while cur_timestep <= 10:
        
            chosen_action = sess.run(output, feed_dict={inputs: get_inputs(cur_timestep)}) # num between 0 and 1
            
            if chosen_action > 0.5:
                chosen_action = 1
            else:
                chosen_action = 0
                
            gotten_num = simulation(chosen_action, cur_timestep)
            
            if gotten_num > 0:
                gotten_numbers.append(gotten_num)
            
            cur_timestep += 1
            print(cur_timestep)
            
        sess.run(backprop, feed_dict={thing_1: get_result(gotten_numbers, 0), thing_2: get_result(gotten_numbers, 1), thing_3: get_result(gotten_numbers, 2)})
        gotten_numbers = [] 
        cur_batch_num += 1
        print("cur_batch_num = ", cur_batch_num)
    
#print("[0.2,0.2,0.7] = ",get_result([0.2,0.2,0.7]))
#print("[0.7,0.7,0.2] = ",get_result([0.7,0.7,0.2]))
#print("[0.5,0.5,0.5] = ",get_result([0.5,0.5,0.5]))
#print("[0.5,0.5,0.7] = ",get_result([0.5,0.5,0.7]))
