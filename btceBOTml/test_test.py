import tensorflow as tf
import math
# trace a line between thing_1 and W1

W1 = tf.Variable(tf.random_normal([1]), name="bill")
x = tf.placeholder(tf.float32, [1])
y_ = tf.multiply(W1, x)
y = tf.constant([25.0])

loss = tf.sqrt(tf.multiply(tf.subtract(y, y_),tf.subtract(y, y_)))
grads = tf.placeholder()

optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.001)#optimizer
ap_grads = optimizer.apply_gradients(zip(grads, tf.trainable_variables))

init = tf.initialize_all_variables()
i = 0

with tf.Session() as sess:
    sess.run(init)
    print("trainable_variables:", sess.run(tf.trainable_variables()))
    while i < 100:
        ys = sess.run(loss, feed_dict={x: [50]})
        tgrads = sess.run(tf.gradients(ys, W1))
        sess.run(ap_grads, feed_dict={grads:tgrads})
        print'when i:', i, 'y_ was:', sess.run(y_, feed_dict={x: [50]}),'and the weight was:', sess.run(W1)
        i += 1
        
