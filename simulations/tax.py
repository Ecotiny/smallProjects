import matplotlib.pyplot as plt
import numpy as np


brackets = [(14000, 0.105),
            (48000, 0.175),
            (70000, 0.300),
            (False, 0.330)]

def gross_to_net(income, debug=False):
    net_income = 0

    for bracket_index in range(len(brackets)):

        bracket = brackets[bracket_index]
        upper_limit = income + 2
        lower_limit = 0

        if debug:
            print(bracket[0], bracket_index)

        if bracket[0]: # upper limit check, False if no upper limit
            upper_limit = bracket[0]

        if bracket_index != 0:
                lower_limit = brackets[bracket_index-1][0] # get upper limit of previous bracket, save as lower limit

        # get amount in certain bracket
        above_lower = income - lower_limit
        bracket_income = 0
        if above_lower > upper_limit - lower_limit:
            bracket_income = upper_limit - lower_limit
        elif above_lower > 0:
            bracket_income = above_lower

        # calculate tax on that
        tax_mult = bracket[1]
        tax = bracket_income * tax_mult

        if debug:
            print("==========")
            print(lower_limit, upper_limit)
            print(bracket_income, tax)


        # add to net income
        net_income += bracket_income - tax

    return net_income

n_samples = 500
max_income = 150000


gross_incomes = np.array(range(n_samples))
gross_incomes = gross_incomes * (max_income / n_samples)
net_incomes = list(map(gross_to_net, gross_incomes))

plt.plot(gross_incomes, gross_incomes, "g-")
plt.plot(gross_incomes, net_incomes, "r-")

plt.ylim(0, max_income)
plt.xlim(0, max_income)
plt.show()
