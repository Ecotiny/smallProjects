def bowler(codenum, curser, string):
    bowl = ""
    for x in range(0, codenum):
        if (x + curser >= len(string)):
            return(bowl)
        else:
            bowl += string[x + curser]
    return bowl
