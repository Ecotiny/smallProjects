import numpy as np
from scipy.optimize import minimize 
import matplotlib.pyplot as plt
from flask import Flask, request

l1 = 100  #mm
l2 = 110  #mm

app = Flask(__name__)

def normalise(v):
    norm = np.linalg.norm(v)
    if norm == 0:
        print("Norm 0")
        return v
    return v / norm

def getCords(a1, a2, plot=False):
    
    x1 = l1 * np.sin(a1)
    y1 = l1 * np.cos(a1)
    
    x2 = l2 * np.sin(a2)
    y2 = l2 * np.cos(a2)
    
    x = x1 + x2
    y = y1 + y2
    
    #if plot:
        #plt.plot([0,x1,x], [0,y1,y], 'bo-')
    
    return (x, y, x1, y1, x2, y2)
    
def getScore(a, tx, ty, prev_a):
    a1, a2 = a
    pa1, pa2 = prev_a
    
    # return absolute end point, mid point, end point rel to midpoint
    cx, cy, x1, y1, x2, y2 = getCords(a1, a2)
    
    #px, py, px1, py1, px2, py2 = getCords(pa1, pa2)
    
    # calculate difference between target and actual endpoint
    
    dist = np.sqrt((tx-cx)**2 + (ty-cy)**2)
    
    # calculate angle difference to minimize movement
    
    # new sections, normalised
    #sec1 = normalise(np.array([x1,y1]))
    #sec2 = normalise(np.array([x2,y2]))
    
    ## previous sections, normalised
    #psec1 = normalise(np.array([px1,py1]))
    #psec2 = normalise(np.array([px2,py2]))
    
    #sec1_diff = np.arccos(np.dot(psec1, sec1))
    #sec2_diff = np.arccos(np.dot(psec2, sec2))
        
    #total_angle_diff = sec1_diff + sec2_diff
    
    #print(total_angle_diff)
    
    #angle_weight = 1
    
    return dist #+ (total_angle_diff * angle_weight)

def move(tx, ty, a1, a2):
    #plt.plot(tx , ty, 'ro')
    res = minimize(getScore, [a1,a2], args=(tx, ty, [a1, a2]))
    print(res.x)
    print(res.nit)
    return res.x

#@app.route("/<float:target_x>/<float:target_y>/<float:current_a1>/<float:current_a2>/")
#def getAngles(target_x, target_y, current_a1, current_a2):
    #print("recieved request")
    #print(request.form)
    #a1, a2 = move(target_x, target_y, current_a1, current_a2)
    #out = "{},{}".format(a1,a2)
    #print(out)
    #return out
    
@app.route("/", methods=["POST"])
def getAngles():
    print(request.form)
    print("recieved request")
    
    data = request.form;
    
    tx = data["target_x"]
    ty = data["target_y"]
    cur_a1 = data["current_a1"]
    cur_a2 = data["current_a2"]
    
    tx = float(tx)
    ty = float(ty)
    cur_a1 = float(cur_a1)
    cur_a2 = float(cur_a2)
    
    a1, a2 = move(tx, ty, cur_a1, cur_a2)
    out = "{},{}".format(a1,a2)
    print(out)
    return out
