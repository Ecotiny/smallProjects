#def simulate(h, wind_resistance, mass):
  #x = h
  #v = 0.0
  #t = 0.0
  #dt = 0.01  # Small steps are more accurate
  
  #while (x > 0):
    #force = wind_resistance*v*v -9.8*mass
    #a = force / mass
    #dv = a*dt
    #dx = v*dt
    
    #t = t + dt   # Step through time
    #v = v + dv
    #x = x + dx
  #return t

#print(simulate(9.81, 0.0, 10))

from numpy import radians
import math
import matplotlib.pyplot as plt

g = 10
th = radians(45)
ath = 0 
vth = 0
dt = 0.01
t = 0
theta = []
velth = []
times = []
while t < 10:
    t = t + dt
    vth = vth + ath*dt
    th = th + vth *dt
    ath = -g*math.sin(th)
    print(th, t)
    times.append(t)
    theta.append(th)
    velth.append(vth)
    plt.plot(times, theta, '*')


plt.xlabel('Time (seconds)')
plt.ylabel('Angle (radians)')
plt.grid(True)
#plt.savefig('test.pdf')
plt.show()
