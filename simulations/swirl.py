from matplotlib import pyplot as plt
import numpy as np
import matplotlib
import sys

sequence = raw_input("Sequence? ").split(",")

#change errytang to an int
for i in range(0, len(sequence)):
    try:
        sequence[i] = int(sequence[i])
    except:
        print "Invalid sequence"
        sys.exit()

direction = -1
coords = [0,0]
timeout = 0
index = 0

plot_x = [0]
plot_y = [0]

done = 0

while timeout < 1000 and done == 0:
    direction += 1  
    
    if direction > 3:
        direction = 0
    
    if direction == 0: #right
        coords[0] = coords[0] + sequence[index]
    elif direction == 1: #down
        coords[1] = coords[1] - sequence[index]
    elif direction == 2: #left
        coords[0] = coords[0] - sequence[index]
    elif direction == 3: #up
        coords[1] = coords[1] + sequence[index]
    
    index += 1
    if index == len(sequence):
        index = 0
    
    timeout += 1
    
    print coords
    
    plot_x.append(coords[0])
    plot_y.append(coords[1])
    
    if coords == [0,0] and index == len(sequence):
        done = 1

plt.plot(plot_x, plot_y)
plt.plot(0,0,'ro')
plt.show()
