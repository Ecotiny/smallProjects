dimensions = int(input("Desired dimensions? (1 number)"))
print("Solving magic square with dimensions of " + str(dimensions) + " by " + str(dimensions) + ", with a range of numbers of 1 to " + str(dimensions**2))
square = []
for i in range(0, dimensions**2):
    square.append(0)

def indexOf(coords):
    return int(coords[0] + ((coords[1] - 1) * dimensions) - 1 )

def changeCoords(coords):
    x = coords[0] + 1
    y = coords[1] - 1
    if y == 0 and x > dimensions:
        y = y + 2
        x = coords[0]
    elif x > dimensions:
        x = 1
    elif y == 0:
        y = dimensions
    elif square[indexOf([x,y])] != 0:
        y = y + 2
        x = coords[0]
    return [x,y]

def printSquare():
    for i in range(0, dimensions**2):
        if i % dimensions == 0:
            print("\n" + str(square[i]) + (" " * (5 - len(str(square[i])))))
        else:
            print(str(square[i]) + (" " * (5 - len(str(square[i])))))
    print(" ")

if dimensions % 2 != 0:
    curCoords = [int ((dimensions+1)/2), 1]
    for num in range(1, (dimensions**2 + 1)):
        square[indexOf(curCoords)] = num
        curCoords = changeCoords(curCoords)

print("Done!")
printSquare()
