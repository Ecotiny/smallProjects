class board_class:
    
    board = []
    width = 0
    height = 0
    
    # directions
    '''
       6  N/A  0
        \  |  / 
         \ | /
          \|/
       5---O---1
          /|\
         / | \
        /  |  \
       4   3   2
    '''
    # index corresponds to direction
    x_add = [1,1,1,0,-1,-1,-1]
    y_add = [1,0,-1,-1,-1,0,1]
    
    def __init__(self, width, height):
        self.board = [[0 for y in range(height)] for x in range(width)]
        self.width = width
        self.height = height
        self.y_add = [1,0,-1,-1,-1,0,1]
        self.x_add = [1,1,1,0,-1,-1,-1]
        print(len(self.y_add))

    
    def check_space(self, x, y, player):

        # directions that contain a piece of the correct color
        dirs = []
        
        for i in range(7):
            x_to_add = self.x_add[i]
            y_to_add = self.y_add[i]
            
            if x + x_to_add > 0 and y + y_to_add > 0 and x + x_to_add < self.width and y + y_to_add < self.height:
                if self.board[x + x_to_add][y + y_to_add] == player:
                    dirs.append(True)
                else:
                    dirs.append(False)
            else:
                dirs.append(False)
        
        found = False
        good_dirs = []
        for i in range(len(dirs)):
            if dirs[i]:
                found = True
                good_dirs.append(i)
        
        won = False
        
        if not found:
            return False
        else:
            for j in good_dirs:
                if self.check_dir(x, y, player, j, 4):
                    return True
                    won = True
            if not won:
                return False
                
            
            
    def check_dir(self, x, y, player, direction, length):
        len_needed = length - 1 #include starting square
        len_found = 0
        
        tmp_x = x
        tmp_y = y
        
        x_to_add = self.x_add[direction]
        y_to_add = self.y_add[direction]
        for i in range(length):
            if len_found == len_needed:
                return True
            if self.board[tmp_x + x_to_add][tmp_y + y_to_add] == player:
                len_found += 1
            tmp_x += x_to_add
            tmp_y += y_to_add
    
    def print_board(self):        
        
        for i in range(self.height)[::-1]:
            string = ""
            for j in range(self.width):
                string += " {}".format(self.board[j][i])
            print string
        
# creation of board

w = 7
h = 6

board_instance = board_class(w,h)

# checker for when game is over
done = 0

# checker for player number/id
player = 2


# game loop
while done == 0:
    
    if player == 2:
        player = 1
    else:
        player = 2
    
    print("Player {}'s turn".format(player))
    board_instance.print_board()
    
    column = ""
    
    x = 0
    y = 0
    
    while type(column) == str:
        column = raw_input("Input column ({} - {}): ".format(1,w))
        try:
            column = int(column)
            x = column - 1 # array indices start at 0
        except(ValueError, TypeError):
            print("Please input a valid number")
        if x > (w - 1) or x < 0:
            print("Column index out of bounds")
    
    done_bottom_check = 0
    for i in range(len(board_instance.board[x])):
        if board_instance.board[x][i] == 0 and done_bottom_check == 0:
            board_instance.board[x][i] = player
            y = i
            done_bottom_check = 1
    
    if board_instance.check_space(x, y, player):
        print("Player {} won!".format(player))
        board_instance.print_board()
        break
            
    
