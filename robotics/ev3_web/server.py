#!/usr/bin/env python3
from flask import Flask
from flask_restful import Resource, Api
from ev3dev2 import motor

#tank_drive = motor.MoveTank('outA', 'outB')

app = Flask(__name__, static_folder='')
api = Api(app)


# Directions:
#
#     1
#     |
#  4--0--2
#     |
#     3
#


@app.route('/<path:path>')
def static_file(path):
    return app.send_static_file(path)

turn_duration = 1
straight_duration = 1

class go(Resource):
    def get(self, dir):
        if dir == 0: # stop
            print("stop")
            #tank_drive.stop()
        elif dir == 1: # forward
            print("forward")
            #tank_drive.on_for_seconds(-100,-100, straight_duration)
        elif dir == 2: # right
            print("right")
            #tank_drive.on_for_seconds(-100,100, turn_duration)
        elif dir == 3: # backwards
            print("back")
            #tank_drive.on_for_seconds(100,100, straight_duration)
        elif dir == 4: # left
            print("left")
            #tank_drive.on_for_seconds(100,-100, turn_duration)

api.add_resource(go, '/dir/<int:dir>')



if __name__ == '__main__':
    app.run(debug=True)
