import matplotlib.pyplot as plt
import csv
import sys
import numpy as np

buyRow = []
buyPri = []
sellRow = []
sellPri = []
buyTotPri = []
buyTotRow = []
sellTotPri = []
sellTotRow = []
fileToCheck = str(sys.argv[1])
i = 1
i2 = 1

with open('buyDataCSV.csv') as csvfile:
     data = csv.DictReader(csvfile)
     for row in data:
        buyRow.append(row['row'])
        buyPri.append(row['buy'])
        
with open('sellDataCSV.csv') as csvfile:
     data = csv.DictReader(csvfile)
     for row in data:
        sellRow.append(row['row'])
        sellPri.append(row['sell'])

with open(fileToCheck) as csvfile:
     data = csv.DictReader(csvfile)
     for row in data:
        buyTotPri.append(float(row['buy']))
        buyTotRow.append(i)
        sellTotPri.append(float(row['sell']))
        sellTotRow.append(i2)
        i2 = i2 + 1
        i = i + 1

#print np.array(sellTotPri)-np.array(buyTotPri)
#plt.plot(buyTotRow, buyTotPri, label="Buy Price")
#plt.plot(sellTotRow, sellTotPri, 'r' , label="Sell Price")
#plt.plot(np.array(sellTotRow), np.array(sellTotPri)-np.array(buyTotPri), label="Difference")
#plt.plot(buyRow, buyPri, 'go', label="Buy")
#plt.plot(sellRow, sellPri, 'yo', label="Sell")

#plt.xlabel("Time/Row (30sec)")
#plt.ylabel("Price of Dash(btc)")
#plt.axes().set_aspect(70000, 'box-forced')
#plt.legend()
#plt.grid(True)
#plt.show()


fig, ax1 = plt.subplots()
ax1.plot(buyTotRow, buyTotPri, label="Buy Price")
ax1.plot(sellTotRow, sellTotPri, 'r' , label="Sell Price")
ax1.plot(buyRow, buyPri, 'go', label="Buy")
ax1.plot(sellRow, sellPri, 'yo', label="Sell")
ax1.set_xlabel('time (30 s)')
# Make the y-axis label, ticks and tick labels match the line color.
ax1.set_ylabel('price', color='b')
ax1.tick_params('y', colors='b')
#ax1.set_aspect(70000, 'box-forced')
ax1.grid(True)

#ax2 = ax1.twinx()
#ax2.plot(np.array(sellTotRow), np.array(sellTotPri)-np.array(buyTotPri), label="Difference")
#ax2.plot(np.array(sellTotRow)[1:-1], np.array(sellTotPri)[0:-2]-np.array(sellTotPri)[1:-1], label="Price change")
#ax2.set_ylabel('Difference', color='r')
#ax2.tick_params('y', colors='r')
##ax2.set_aspect(7000, 'box-forced')
#ax2.grid(True)

fig.tight_layout()
plt.legend()
plt.show()
