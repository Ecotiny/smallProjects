# foo = [1,2,3,4,5,6,7,8,9]

def write2csv(array, csvFile)
    string = ''
    array.each() do |blah|
        string << blah.to_s + ", \t"
    end
    string = string[0...-3]
    open(csvFile, 'a') { |f|
        f.puts string.to_s
        
    }
end

def write2csvMultRow(array, csvFile, cutoffNum)
    x = 0
    while x <= array.length
        if ((x % cutoffNum) == 0)
            tempArray = array[x..x+(cutoffNum-1)]
            write2csv(tempArray, csvFile)
        end
        x = x + 1
    end
end


# write2csvMultRow(foo, 'paraID.csv', 3)
# write2csv(foo, 'paraID.csv')
