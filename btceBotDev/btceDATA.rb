require 'btce'
require 'json'
require 'mysql2'
require 'timers'

times = Timers::Group.new

@client = Mysql2::Client.new(:host => "localhost", :username => "linus", :password => "linus" , :database => "coindata")
i = 0
done = 0
latest = @client.query("SELECT MAX(time) FROM dsh_btc").to_a;
#prepare
insert_breaks = @client.prepare("INSERT INTO breaks (time) VALUES (?)")

begin 
    puts latest[0]["MAX(time)"]
    if latest[0]["MAX(time)"] < (Time.new - 2.minutes) then
        insert_breaks.execute(latest[0]["MAX(time)"])
    end
rescue
    puts "Error"
end

insert_dsh_btc = @client.prepare('INSERT INTO dsh_btc(sell,buy,vol_cur,vol,time) VALUES (?,?,?,?,?)')
insert_dsh_ltc = @client.prepare('INSERT INTO dsh_ltc(sell,buy,vol_cur,vol,time) VALUES (?,?,?,?,?)')
insert_dsh_eth = @client.prepare('INSERT INTO dsh_eth(sell,buy,vol_cur,vol,time) VALUES (?,?,?,?,?)')
insert_dsh_usd = @client.prepare('INSERT INTO dsh_usd(sell,buy,vol_cur,vol,time) VALUES (?,?,?,?,?)')
insert_btc_usd = @client.prepare('INSERT INTO btc_usd(sell,buy,vol_cur,vol,time) VALUES (?,?,?,?,?)')
insert_ltc_usd = @client.prepare('INSERT INTO ltc_usd(sell,buy,vol_cur,vol,time) VALUES (?,?,?,?,?)')
insert_eth_usd = @client.prepare('INSERT INTO eth_usd(sell,buy,vol_cur,vol,time) VALUES (?,?,?,?,?)')
insert_ltc_btc = @client.prepare('INSERT INTO ltc_btc(sell,buy,vol_cur,vol,time) VALUES (?,?,?,?,?)')
insert_eth_btc = @client.prepare('INSERT INTO eth_btc(sell,buy,vol_cur,vol,time) VALUES (?,?,?,?,?)')
insert_eth_ltc = @client.prepare('INSERT INTO eth_ltc(sell,buy,vol_cur,vol,time) VALUES (?,?,?,?,?)')


wait_interval = times.every(30) {begin
        ticker = Btce::Ticker.new "dsh_btc-dsh_ltc-dsh_eth-dsh_usd-btc_usd-ltc_usd-eth_usd-ltc_btc-eth_btc-eth_ltc"
        ticker_JSON = JSON.parse(ticker.json.to_json)
        
        #INSERTATIONS
        
        insert_dsh_btc.execute(ticker_JSON['dsh_btc']['sell'].to_s,ticker_JSON['dsh_btc']['buy'].to_s,ticker_JSON['dsh_btc']['vol_cur'].to_s,ticker_JSON['dsh_btc']['vol'].to_s,Time.new.to_s)
        insert_dsh_ltc.execute(ticker_JSON['dsh_ltc']['sell'].to_s,ticker_JSON['dsh_ltc']['buy'].to_s,ticker_JSON['dsh_ltc']['vol_cur'].to_s,ticker_JSON['dsh_ltc']['vol'].to_s,Time.new.to_s)
        insert_dsh_eth.execute(ticker_JSON['dsh_eth']['sell'].to_s,ticker_JSON['dsh_eth']['buy'].to_s,ticker_JSON['dsh_eth']['vol_cur'].to_s,ticker_JSON['dsh_eth']['vol'].to_s,Time.new.to_s)
        insert_dsh_usd.execute(ticker_JSON['dsh_usd']['sell'].to_s,ticker_JSON['dsh_usd']['buy'].to_s,ticker_JSON['dsh_usd']['vol_cur'].to_s,ticker_JSON['dsh_usd']['vol'].to_s,Time.new.to_s)
        insert_btc_usd.execute(ticker_JSON['btc_usd']['sell'].to_s,ticker_JSON['btc_usd']['buy'].to_s,ticker_JSON['btc_usd']['vol_cur'].to_s,ticker_JSON['btc_usd']['vol'].to_s,Time.new.to_s)
        insert_ltc_usd.execute(ticker_JSON['ltc_usd']['sell'].to_s,ticker_JSON['ltc_usd']['buy'].to_s,ticker_JSON['ltc_usd']['vol_cur'].to_s,ticker_JSON['ltc_usd']['vol'].to_s,Time.new.to_s)
        insert_eth_usd.execute(ticker_JSON['eth_usd']['sell'].to_s,ticker_JSON['eth_usd']['buy'].to_s,ticker_JSON['eth_usd']['vol_cur'].to_s,ticker_JSON['eth_usd']['vol'].to_s,Time.new.to_s)
        insert_ltc_btc.execute(ticker_JSON['ltc_btc']['sell'].to_s,ticker_JSON['ltc_btc']['buy'].to_s,ticker_JSON['ltc_btc']['vol_cur'].to_s,ticker_JSON['ltc_btc']['vol'].to_s,Time.new.to_s)
        insert_eth_btc.execute(ticker_JSON['eth_btc']['sell'].to_s,ticker_JSON['eth_btc']['buy'].to_s,ticker_JSON['eth_btc']['vol_cur'].to_s,ticker_JSON['eth_btc']['vol'].to_s,Time.new.to_s)
        insert_eth_ltc.execute(ticker_JSON['eth_ltc']['sell'].to_s,ticker_JSON['eth_ltc']['buy'].to_s,ticker_JSON['eth_ltc']['vol_cur'].to_s,ticker_JSON['eth_ltc']['vol'].to_s,Time.new.to_s)
        
        done = done + 1
#         i = 0
        puts "Collected " + done.to_s + " * 30 sec of data"
    rescue Exception => ex
        puts "An error of type #{ex.class} happened, message is #{ex.message}"
    end}

loop {times.wait}
# while TRUE do
#     if i == 30 then
#         begin
#             ticker = Btce::Ticker.new "dsh_btc-dsh_ltc-dsh_eth-dsh_usd-btc_usd-ltc_usd-eth_usd-ltc_btc-eth_btc-eth_ltc"
#             ticker_JSON = JSON.parse(ticker.json.to_json)
#             
#             #INSERTATIONS
#             
#             insert_dsh_btc.execute(ticker_JSON['dsh_btc']['sell'].to_s,ticker_JSON['dsh_btc']['buy'].to_s,ticker_JSON['dsh_btc']['vol_cur'].to_s,ticker_JSON['dsh_btc']['vol'].to_s,Time.new.to_s)
#             insert_dsh_ltc.execute(ticker_JSON['dsh_ltc']['sell'].to_s,ticker_JSON['dsh_ltc']['buy'].to_s,ticker_JSON['dsh_ltc']['vol_cur'].to_s,ticker_JSON['dsh_ltc']['vol'].to_s,Time.new.to_s)
#             insert_dsh_eth.execute(ticker_JSON['dsh_eth']['sell'].to_s,ticker_JSON['dsh_eth']['buy'].to_s,ticker_JSON['dsh_eth']['vol_cur'].to_s,ticker_JSON['dsh_eth']['vol'].to_s,Time.new.to_s)
#             insert_dsh_usd.execute(ticker_JSON['dsh_usd']['sell'].to_s,ticker_JSON['dsh_usd']['buy'].to_s,ticker_JSON['dsh_usd']['vol_cur'].to_s,ticker_JSON['dsh_usd']['vol'].to_s,Time.new.to_s)
#             insert_btc_usd.execute(ticker_JSON['btc_usd']['sell'].to_s,ticker_JSON['btc_usd']['buy'].to_s,ticker_JSON['btc_usd']['vol_cur'].to_s,ticker_JSON['btc_usd']['vol'].to_s,Time.new.to_s)
#             insert_ltc_usd.execute(ticker_JSON['ltc_usd']['sell'].to_s,ticker_JSON['ltc_usd']['buy'].to_s,ticker_JSON['ltc_usd']['vol_cur'].to_s,ticker_JSON['ltc_usd']['vol'].to_s,Time.new.to_s)
#             insert_eth_usd.execute(ticker_JSON['eth_usd']['sell'].to_s,ticker_JSON['eth_usd']['buy'].to_s,ticker_JSON['eth_usd']['vol_cur'].to_s,ticker_JSON['eth_usd']['vol'].to_s,Time.new.to_s)
#             insert_ltc_btc.execute(ticker_JSON['ltc_btc']['sell'].to_s,ticker_JSON['ltc_btc']['buy'].to_s,ticker_JSON['ltc_btc']['vol_cur'].to_s,ticker_JSON['ltc_btc']['vol'].to_s,Time.new.to_s)
#             insert_eth_btc.execute(ticker_JSON['eth_btc']['sell'].to_s,ticker_JSON['eth_btc']['buy'].to_s,ticker_JSON['eth_btc']['vol_cur'].to_s,ticker_JSON['eth_btc']['vol'].to_s,Time.new.to_s)
#             insert_eth_ltc.execute(ticker_JSON['eth_ltc']['sell'].to_s,ticker_JSON['eth_ltc']['buy'].to_s,ticker_JSON['eth_ltc']['vol_cur'].to_s,ticker_JSON['eth_ltc']['vol'].to_s,Time.new.to_s)
#             
#             done = done + 1
#             i = 0
#             puts "Collected " + done.to_s + " * 30 sec of data"
#         rescue
#             puts "Error"
#         end
#     end
#     sleep 1
#     i=i+1
# end
 
