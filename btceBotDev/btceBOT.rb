require 'btce'
require 'csv'
$transFee = 0.2.to_f #this is in percent
# --------------------------------------------------------------------------------------------------------------
#                   list of variables that are being used in "fake program"                                    |     
# $rowNum = the current row number                                                                             | 
# $highestBuy = the highest Buy value (reset after each buy sell to $fakeBuy)                                  |
# $highestSell = the highest Sell value (reset after each buy sell to $fakeSell)                               |     
# $hasBeen = used in shouldBuy() to determine if $fakeBuy has been below 2% of highestBuy                      |             
# $fakeBalBTC = fake bitcoin balance set at top                                                                |
# $fakeBalDSH = fake dashcoin balance set at top                                                               | 
# $fakeBuy = the buy price                                                                                     | 
# $fakeSell = the sell price                                                                                   |  
# $greedynessDecimal = how high onUpSellThreshold() goes before being just 1% below highestSell                | 
# $boughtSell = the price of sell at when a transaction occurs                                                 |
# $boughtBuy = the price of buy at when a transaction occurs                                                   | 
# --------------------------------------------------------------------------------------------------------------

# logistics of buy and sell csv/arrays. arrays first then make them csvs
# to arrays created: sellDataArray, and buyDataArray.
# w/ each sell, append the $fakeSell and $rowNum to sellDataArray in their own rowNum
# same goes for buyDataArray.
# methods used to add this data to the arrays

File.truncate('sellDataCSV.csv', 9)
File.truncate('buyDataCSV.csv', 8)

open('buyDataCSV.csv', 'a') { |g|
    g.puts ''
}
open('sellDataCSV.csv', 'a') { |h|
    h.puts ''
}

$fakeBalBTC = 1
$fakeBalDSH = 0

# def getFromTicker(currency_pair, variable)
#     getFromTickerVar = Btce::Ticker.new currency_pair
#     getFromTickerVar = JSON.parse(getFromTickerVar.json.to_json)
#     getFromTickerVar = getFromTickerVar[currency_pair][variable]
#     return getFromTickerVar
# end

# testTrade = Btce::Trade.new_from_json("Hi")
# puts testTrade.to_json

$rowNum = 1
csvToCheck = ARGV[0].to_s
$csv = CSV.new(open(csvToCheck, "r")).to_a
$lowestBuy = $csv[$rowNum][1].to_f 
$highestSell = $csv[$rowNum][2].to_f 
$boughtBuy = $fakeBuy.to_f
$boughtSell = $fakeSell.to_f
$greedynessDecimal = 0.03 # used in onUpSellThreshold method 


def haveBits()
    info = Btce::TradeAPI.new_from_keyfile.get_info
    info_JSON = JSON.parse(info.to_json)
    if info_JSON["return"]["funds"]["btc"] > 0
        return TRUE
    else
        return FALSE
    end
end

def haveDash()
    info = Btce::TradeAPI.new_from_keyfile.get_info
    info_JSON = JSON.parse(info.to_json)
    if info_JSON["return"]["funds"]["dsh"] > 0
        return TRUE
    else
        return FALSE
    end
end

def currentDashPrice() #not being used at the moment
    $fakeBuy = $csv[$rowNum][1].to_f
    return $fakeBuy
end

$hasBeen = 0

def shouldBuy()
# new idea for logic behind this. 1.5% above lowest buy value    
    $lowestBuy
    upValue = $lowestBuy*1.015
    
    if $fakeBuy >= upValue
        return TRUE
    end
    
    return FALSE
    
#     $fakeBuy = currentDashPrice() #getFromTicker('dsh_btc', 'buy')
#     downValue = $highestBuy*0.98.to_f
#     upValue = $highestBuy*0.99.to_f
#         
#     if $fakeBuy <= downValue 
#         $hasBeen = 1
#         return FALSE
#     end
#     
#     if $hasBeen == 1 and $fakeBuy >= upValue
#         $hasBeen = 0
#         return TRUE
#     end
#     
#     return FALSE
end

def onUpSellThreshold()
    
    $highestSell = [$highestSell, $fakeSell].max
    
    if $boughtSell*(1+(transFeeCalc()/100)) < $highestSell and $highestSell < ($boughtSell*(1+$greedynessDecimal))*(1+(transFeeCalc()/100))
        sellThreshold = (($highestSell-($boughtSell*(1+transFeeCalc()/100)))/2) + $boughtSell*(1+(transFeeCalc()/100))
    elsif $highestSell >= ($boughtSell*(1+$greedynessDecimal))*(1+transFeeCalc()/100)
        sellThreshold = $highestSell-($boughtSell*($greedynessDecimal/2))
    end
    return sellThreshold
end
    
def shouldSell()
    
    $boughtBuy
    onDownSell = $boughtBuy*0.98
    aLittleAboveFeeFloor = 1.002
    
    if $fakeSell > $boughtBuy*aLittleAboveFeeFloor*(1+(transFeeCalc()/100))
        if $fakeSell <= onUpSellThreshold()
            return TRUE
        end
    elsif $fakeSell <= onDownSell 
        return FALSE
    end
    $highestSell = [$highestSell, $fakeSell].max
    return FALSE
end

def buy()
#     research how to do this w/ trade api and trade all funds to dash at trade price determined by should buy
end

def fakeBuy()
    $fakeBalDSH = $fakeBalBTC/$fakeBuy.to_f*(1-(transFeeCalc()/100))
    $fakeBalBTC = 0
    $boughtBuy = $fakeBuy 
    $boughtSell = $fakeSell
    $highestSell  = $boughtSell
    puts "i have bought dash on line: " + $rowNum.to_s
    puts "dsh bal: " + $fakeBalDSH.to_s
    puts " "
    
    open('buyDataCSV.csv', 'a') { |f| 
            f.puts $fakeBuy.to_s + "," + $rowNum.to_s
    }
#     puts "-----------------------------"
end

def sell()
#     sell all dash at price defined by should sell function
end

def fakeSell()
    $fakeBalBTC = $fakeBalDSH*$fakeSell*(1-(transFeeCalc()/100))
    $fakeBalDSH = 0
    $boughtBuy = $fakeBuy 
    $boughtSell = $fakeSell
    $lowestBuy  = $boughtBuy
    puts "i have sold dash on line: " + $rowNum.to_s
    puts "btc bal: " + $fakeBalBTC.to_s
    puts "-----------------------------"
    
    open('sellDataCSV.csv', 'a') { |f| 
            f.puts $fakeSell.to_s + "," + $rowNum.to_s
    }
    
end

def transFeeCalc() 
    transOne = (100+$transFee)/(100-$transFee)
    priceFloor = (transOne-1)*100 #output in percent
    return priceFloor
#     to be used just in case the fee changes a lot
end

def fakeHaveBits()
	if $fakeBalBTC >= 0.00001
	    return TRUE
	else
	    return FALSE
	end
end

def fakeHaveDash()
	if $fakeBalDSH >= 0.000000001 
	    return TRUE
	else
	    return FALSE
	end
end

def addToSellDataArray(sellprice, rowNumber)
    
end

# def convertMoneyToValueInBit()
#     if haveBits()
#         yayMoney = $fakeBalBTC
#     elsif haveDash()
#        yayMoney = $fakeBalDSH/$fakeSell
#     else
#         yayMoney = 0
#     end
#     return yayMoney
# end

# puts "opening Value in Bitcoin : " + $fakeBalBTC.to_s#convertMoneyToValueInBit().to_s
puts "opening Bits: " + $fakeBalBTC.to_s
puts "opening Dash: " + $fakeBalDSH.to_s
puts "-----------------------------"

while $csv.length > $rowNum do 
    
    $fakeBuy = $csv[$rowNum][1].to_f
    $fakeSell = $csv[$rowNum][0].to_f
    
    $lowestBuy = [$lowestBuy, $fakeBuy].min
#     puts $rowNum.to_s + ", " + $hasBeen.to_s + ", " + $fakeBuy.to_s
    
    if fakeHaveBits() 
        #puts "i have bitcoin right now"
        if shouldBuy() 
            fakeBuy()
        else
#             puts "this is not the right time to buy dash"
        end
    elsif fakeHaveDash()
        #puts "i have dash right now"
        if shouldSell()
            fakeSell()
        else
            #puts "this is not the right time to sell dash"
        end
    else
        #puts "you have neither dash nor bitcoin"
    end
#     puts "dash balance: " + $fakeBalDSH.to_s
#     puts "bitcoin balance: " + $fakeBalBTC.to_s
#     puts "buy price: " + $fakeBuy.to_s
#     puts "sell price: " + $fakeSell.to_s
#     puts "----------"
    $rowNum = $rowNum + 1
    
#     break if $rowNum > 300

end

puts "-----------------------------"
puts "closing Bits: " + $fakeBalBTC.to_s
puts "closing Dash: " + $fakeBalDSH.to_s
# puts "closing Value in Bitcoin : " + convertMoneyToValueInBit().to_s
puts "final row: " + $rowNum.to_s
puts "done"
