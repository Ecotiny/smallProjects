drop database if exists coindata;
create database coindata;

DROP USER 'linus'@'localhost';

FLUSH PRIVILEGES;

CREATE USER 'linus'@'localhost' IDENTIFIED BY 'linus';

GRANT ALL PRIVILEGES ON coindata.* TO 'linus'@'localhost' IDENTIFIED BY 'linus';

FLUSH PRIVILEGES;
