USE `coindata`

DROP TABLE IF EXISTS `dsh_btc`;

CREATE TABLE IF NOT EXISTS `dsh_btc` (
  `sell` float(11) NULL,
  `buy` float(11) NULL,
  `vol` float(11) NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `vol_cur` float(11) NULL
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `dsh_ltc`;

CREATE TABLE IF NOT EXISTS `dsh_ltc` (
  `sell` float(11) NULL,
  `buy` float(11) NULL,
  `vol` float(11) NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `vol_cur` float(11) NULL
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `dsh_eth`;

CREATE TABLE IF NOT EXISTS `dsh_eth` (
  `sell` float(11) NULL,
  `buy` float(11) NULL,
  `vol` float(11) NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `vol_cur` float(11) NULL
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `dsh_usd`;

CREATE TABLE IF NOT EXISTS `dsh_usd` (
  `sell` float(11) NULL,
  `buy` float(11) NULL,
  `vol` float(11) NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `vol_cur` float(11) NULL
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `btc_usd`;

CREATE TABLE IF NOT EXISTS `btc_usd` (
  `sell` float(11) NULL,
  `buy` float(11) NULL,
  `vol` float(11) NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `vol_cur` float(11) NULL
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `ltc_usd`;

CREATE TABLE IF NOT EXISTS `ltc_usd` (
  `sell` float(11) NULL,
  `buy` float(11) NULL,
  `vol` float(11) NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `vol_cur` float(11) NULL
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `eth_usd`;

CREATE TABLE IF NOT EXISTS `eth_usd` (
  `sell` float(11) NULL,
  `buy` float(11) NULL,
  `vol` float(11) NULL,
    `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `vol_cur` float(11) NULL  
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `ltc_btc`;

CREATE TABLE IF NOT EXISTS `ltc_btc` (
  `sell` float(11) NULL,
  `buy` float(11) NULL,
  `vol` float(11) NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `vol_cur` float(11) NULL
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `eth_btc`;

CREATE TABLE IF NOT EXISTS `eth_btc` (
  `sell` float(11) NULL,
  `buy` float(11) NULL,
  `vol` float(11) NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `vol_cur` float(11) NULL
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `eth_ltc`;

CREATE TABLE IF NOT EXISTS `eth_ltc` (
  `sell` float(11) NULL,
  `buy` float(11) NULL,
  `vol` float(11) NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `vol_cur` float(11) NULL
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `eth_ltc`;

CREATE TABLE IF NOT EXISTS `eth_ltc` (
  `sell` float(11) NULL,
  `buy` float(11) NULL,
  `vol` float(11) NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `vol_cur` float(11) NULL
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `breaks`;

CREATE TABLE IF NOT EXISTS `breaks` (
  `time` varchar(30) NULL,
  `id` int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY
) ENGINE=InnoDB;

-- ----------------
-- ADD OLD CSV DATA
-- ----------------

-- INSERT INTO breaks (time) VALUES ("2017-04-16 16:23:15");

-- LOAD DATA LOCAL INFILE '../../data/btc_usd.csv' 
-- INTO TABLE btc_usd
-- FIELDS TERMINATED BY ',' 
-- ENCLOSED BY '"'
-- LINES TERMINATED BY '\n';
-- 
-- LOAD DATA LOCAL INFILE '../../data/dsh_btc.csv' 
-- INTO TABLE dsh_btc
-- FIELDS TERMINATED BY ',' 
-- ENCLOSED BY '"'
-- LINES TERMINATED BY '\n';
-- 
-- LOAD DATA LOCAL INFILE '../../data/dsh_eth.csv' 
-- INTO TABLE dsh_eth
-- FIELDS TERMINATED BY ',' 
-- ENCLOSED BY '"'
-- LINES TERMINATED BY '\n';
-- 
-- LOAD DATA LOCAL INFILE '../../data/dsh_ltc.csv' 
-- INTO TABLE dsh_ltc
-- FIELDS TERMINATED BY ',' 
-- ENCLOSED BY '"'
-- LINES TERMINATED BY '\n';
-- 
-- LOAD DATA LOCAL INFILE '../../data/dsh_usd.csv' 
-- INTO TABLE dsh_usd
-- FIELDS TERMINATED BY ',' 
-- ENCLOSED BY '"'
-- LINES TERMINATED BY '\n';
-- 
-- LOAD DATA LOCAL INFILE '../../data/eth_btc.csv' 
-- INTO TABLE eth_btc
-- FIELDS TERMINATED BY ',' 
-- ENCLOSED BY '"'
-- LINES TERMINATED BY '\n';
-- 
-- LOAD DATA LOCAL INFILE '../../data/eth_ltc.csv' 
-- INTO TABLE eth_ltc
-- FIELDS TERMINATED BY ',' 
-- ENCLOSED BY '"'
-- LINES TERMINATED BY '\n';
-- 
-- LOAD DATA LOCAL INFILE '../../data/eth_usd.csv' 
-- INTO TABLE eth_usd
-- FIELDS TERMINATED BY ',' 
-- ENCLOSED BY '"'
-- LINES TERMINATED BY '\n';
-- 
-- LOAD DATA LOCAL INFILE '../../data/ltc_btc.csv' 
-- INTO TABLE ltc_btc
-- FIELDS TERMINATED BY ',' 
-- ENCLOSED BY '"'
-- LINES TERMINATED BY '\n';
-- 
-- LOAD DATA LOCAL INFILE '../../data/ltc_usd.csv' 
-- INTO TABLE ltc_usd
-- FIELDS TERMINATED BY ',' 
-- ENCLOSED BY '"'
-- LINES TERMINATED BY '\n';
