require 'btce'
require 'csv'
require 'writeToCsv'
# load 'paraThreshold.rb'

$transFee = 0.2.to_f #this is in percent
# --------------------------------------------------------------------------------------------------------------
#                   list of variables that are being used in "fake program"                                    |     
# $rowNum = the current row number                                                                             | 
# $highestBuy = the highest Buy value (reset after each buy sell to $fakeBuy)                                  |
# $highestSell = the highest Sell value (reset after each buy sell to $fakeSell)                               |     
# $hasBeen = used in shouldBuy() to determine if $fakeBuy has been below 2% of highestBuy                      |             
# $fakeBalBTC = fake bitcoin balance set at top                                                                |
# $fakeBalDSH = fake dashcoin balance set at top                                                               | 
# $fakeBuy = the buy price                                                                                     | 
# $fakeSell = the sell price                                                                                   |  
# $greedynessDecimal = how high onUpSellThreshold() goes before being just 1% below highestSell                | 
# $boughtSell = the price of sell at when a transaction occurs                                                 |
# $boughtBuy = the price of buy at when a transaction occurs                                                   | 
# --------------------------------------------------------------------------------------------------------------

# logistics of buy and sell csv/arrays. arrays first then make them csvs
# to arrays created: sellDataArray, and buyDataArray.
# w/ each sell, append the $fakeSell and $rowNum to sellDataArray in their own rowNum
# same goes for buyDataArray.
# methods used to add this data to the arrays

$graph = FALSE 

if ($graph)
    File.truncate('sellDataCSV.csv', 9)
    File.truncate('buyDataCSV.csv', 8)

    open('buyDataCSV.csv', 'a') { |g|
        g.puts ''
    }
    open('sellDataCSV.csv', 'a') { |h|
        h.puts ''
    }
end


# def getFromTicker(currency_pair, variable)
#     getFromTickerVar = Btce::Ticker.new currency_pair
#     getFromTickerVar = JSON.parse(getFromTickerVar.json.to_json)
#     getFromTickerVar = getFromTickerVar[currency_pair][variable]
#     return getFromTickerVar
# end


# csvToCheck = ARGV[0].to_s
# $csv = CSV.new(open(csvToCheck, "r")).to_a
# $lowestBuy = $csv[$rowNum][1].to_f 
# $highestSell = $csv[$rowNum][2].to_f 


def haveBits()
    info = Btce::TradeAPI.new_from_keyfile.get_info
    info_JSON = JSON.parse(info.to_json)
    if info_JSON["return"]["funds"]["btc"] > 0
        return TRUE
    else
        return FALSE
    end
end

def haveDash()
    info = Btce::TradeAPI.new_from_keyfile.get_info
    info_JSON = JSON.parse(info.to_json)
    if info_JSON["return"]["funds"]["dsh"] > 0
        return TRUE
    else
        return FALSE
    end
end

def currentDashPrice() #not being used at the moment
    $fakeBuy = $csv[$rowNum][1]
    return $fakeBuy
end

$hasBeen = 0

def shouldBuy()
    #1.5% above lowest buy value    
    upValue = $lowestBuy*1.015
    
    if $fakeBuy >= upValue
        return TRUE
    end
    
    return FALSE
    
end

def onUpSellThreshold(b, c, d)
    a = $boughtSell/(b**2 + c) # do this before we turn c to a usable variable
    c = $boughtSell*c # because c is a fraction 

    x = $rowNum - $boughtRow
    
    if (x < d)
        return  a*(x - b) + c
    else
        return $highestSell-($boughtSell*0.01)
    end
end
    
def shouldSell(b, c, d)
    
    $boughtBuy
    onDownSell = $boughtBuy*0.98
    aLittleAboveFeeFloor = 1.002
    
    if $fakeSell > $boughtBuy*aLittleAboveFeeFloor*(1+(priceFloorCalc()/100))
        if $fakeSell <= onUpSellThreshold(b, c, d)
            return TRUE
        end
    elsif $fakeSell <= onDownSell 
        return FALSE
    end
    $highestSell = [$highestSell, $fakeSell].max
    return FALSE
end

def buy()
end


def fakeBuy()
    $fakeBalDSH = subtract_transaction_fee($fakeBalBTC/$fakeBuy)
    $fakeBalBTC = 0
    $boughtBuy = $fakeBuy 
    $boughtSell = $fakeSell
    $highestSell  = $boughtSell
    $boughtRow = $rowNum
#     puts "i have bought dash on line: " + $rowNum.to_s
#     puts "dsh bal: " + $fakeBalDSH.to_s
#     puts " "
    if ($graph)
        open('buyDataCSV.csv', 'a') { |f| 
            f.puts $fakeBuy.to_s + "," + $rowNum.to_s
        }
    end
end

def sell()
end

def subtract_transaction_fee(amount)
    return amount*(1 - $transFee/100)
end

def fakeSell()
    $fakeBalBTC = subtract_transaction_fee($fakeBalDSH*$fakeSell)
    
    $fakeBalDSH = 0
    $boughtBuy = $fakeBuy 
    $boughtSell = $fakeSell
    $lowestBuy  = $boughtBuy
#     puts "i have sold dash on line: " + $rowNum.to_s
#     puts "btc bal: " + $fakeBalBTC.to_s
#     puts "-----------------------------"
    if $graph
        open('sellDataCSV.csv', 'a') { |f| 
                f.puts $fakeSell.to_s + "," + $rowNum.to_s
        }
    end
end

def priceFloorCalc() 
    transOne = (100+$transFee)/(100-$transFee)
    priceFloor = (transOne-1)*100 #output in percent
    return priceFloor
end

def fakeHaveBits()
	if $fakeBalBTC >= 0.00001
	    return TRUE
	else
	    return FALSE
	end
end

def fakeHaveDash()
	if $fakeBalDSH >= 0.000000001 
	    return TRUE
	else
	    return FALSE
	end
end

def btc_balance()
    return $fakeBalBTC if ($fakeBalBTC > 0.0)
    return $fakeBalDSH*$fakeSell
end


def paraBot(adjustID, b, c, d, csv)
#     $boughtBuy = $fakeBuy.to_f
#     $boughtSell = $fakeSell.to_f
    $fakeBalBTC = 1
    $fakeBalDSH = 0
    $rowNum = 1
    $csv = csv
    $lowestBuy = $csv[$rowNum][1]
    $highestSell = $csv[$rowNum][2]
    $runThruNum = 1
    $rowNum = 1
    tempArray = []
    openingBal = btc_balance()

    $csv.each() do |row|
        $fakeSell = row[0]
        $fakeBuy = row[1]
        $lowestBuy = [$lowestBuy, $fakeBuy].min
        
        if fakeHaveBits() 
            if shouldBuy() 
                fakeBuy()
            end
        elsif fakeHaveDash()
            if shouldSell(b, c, d)
                fakeSell()
            end
        end
        $rowNum = $rowNum + 1
   end

    closingBal = btc_balance()
    tempArray = [adjustID, b, c, d, openingBal, closingBal]
    write2csv(tempArray, 'paraResults.csv')
    return closingBal
end
