#a is the multipler 
#b is the negaive x adjuster
#c is the y adjuster
#c is the 1% point(defined on the x axis/rowNum)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# INPUT TO SYSTEM                                                                                             |
    #boughtSell - btceBOT                                                                                     |
    #fakeSell - btceBOT                                                                                       |
    #rowNum - btceBOT                                                                                         |
    #runThruNum(directly related to adjustID) - btceBOT                                                       |
# OUTPUT OF SYSTEM                                                                                            |
    #paraThreshold w/ relevant "adjustID"(the num that represents what set of a, b, c, d values it is using)  |
    #the btceBOT file will feed the adjustID, openingBal and closingBal to paraThresholdAnalyser              | 
    #change the paraID.csv so it has the adjustIDs and the corresponding a, b, c, d values                    |
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

$bLimit = 200 #max row nums b goes along
$dLimit = 2*$bLimit #max row nums d can be

def linspace(start, stop, n_steps)
    ret = []
    x = start
    dx = (stop-start)/(n_steps-1)
    n_steps.times() do
        ret << x
        x += dx
    end
    return ret
end



def numbersToUseGen(n_steps)
    # only really change b, c, d because a is a result of what b and c are using the equation yIntercept(boughtSell) = ab^2+c
    # if we rearrage yIntercept = ab^2+c to be for a it becomes a = yIntercept/(b^2+c)
    adjustIDArray = []
    bLimit = 200
    dLimit = 500
    adjustID = 1
    linspace(0.0, bLimit, n_steps).each do |b|
        linspace(0.0, 1.5, n_steps).each do |c| # output in a decimal 
            linspace(0.0, dLimit, n_steps).each do |d|
                adjustIDArray << [adjustID,b,c,d]
                adjustID = adjustID + 1
            end
        end
    end
    return adjustIDArray 
end

# puts numbersToUseGen(5)
