#
# To run this remember to put -I . on the command line, like so
#
# ruby -I . main.rb
#
require 'paraBtceBOT'
require 'paraThreshold'
require 'writeToCsv'
require 'csv'


csv = CSV.new(open('dsh_btc_test1.csv', "r")).to_a
csv2 = []
r = 1
while r < csv.length
    csv2 << [csv[r][0].to_f, csv[r][1].to_f]
    r = r + 1
end

#with n_steps at 3, it took 00:00:13 which is aprox 2.07 parabolas/sec
#with n_steps at 4, it took 00:00:48 which is aprox 1.33 parabolas/sec
#with n_steps at 5, it took 00:02:12 which is aprox 0.95 parabolas/sec
#with n_steps at 6, it took 00:05:30 which is aprox 0.65 parabolas/sec

n_steps = 20

data = numbersToUseGen(n_steps)

File.truncate('paraID.csv', 0)
File.truncate('paraResults.csv', 0)
write2csv(['adjustID','b', 'c', 'd', 'openingBal','closingBal'], 'paraResults.csv')

t1 = Time.now
# processing...

best = [0,0,0,0]

n = 0
r = Random.new
while TRUE do
    b = r.rand(50.0)
    c = r.rand(1.0)
    d = r.rand(1000.0) + 50.0
    n = n + 1
    closing,a = paraBot(n, b, c, d, csv2)
    if (closing > best[0]) 
         best = [closing, n, b, c, d]
        puts "#{best}"
    end
    print("#{n}     \r")
end

data.each() do |row|
    adjustID, b,c,d = row
    print("parabola: #{(100*adjustID.to_f/(n_steps**3)).round(2)}% complete\r")
    closing,a = paraBot(adjustID, b, c, d, csv2)
    if (closing > best[0]) 
         best = [closing, adjustID, b, c, d]
        puts "#{best}"
    end

end
puts "Best #{best}"

t2 = Time.now
delta = t2 - t1
puts "n_steps at #{n_steps}, it took #{delta.round(2)}s. which is aprox #{(data.length() / delta).round(2)} parabolas/sec"
