import predict
import telepot
from telepot.loop import MessageLoop
from telepot.delegate import per_chat_id, create_open, pave_event_space
import urllib2
import time

class chatter(telepot.helper.ChatHandler):
    def __init__(self, *args, **kwargs):
        super(chatter, self).__init__(*args, **kwargs)
        self.qth = False
        self.sats = {"METEOR M2" : "https://celestrak.com/cgi-bin/TLE.pl?CATNR=40069", "NOAA 15" : "https://celestrak.com/cgi-bin/TLE.pl?CATNR=25338", "NOAA 18" : "https://celestrak.com/cgi-bin/TLE.pl?CATNR=28654", "NOAA 19" : "https://celestrak.com/cgi-bin/TLE.pl?CATNR=33591"}

    def open(self, initial_msg, seed):
        self.sender.sendMessage('Send a TLE (format: "tle <TLE>") \n Get passes from one of our preprogrammed sats (format: "pre <sat name>") \n Before doing either of those, you must update your location (format: "loc <north latitude>,<west longitude>,<altitude>")')
        return True  # prevent on_message() from being called on the initial message

    def on_chat_message(self, msg):
        content_type, chat_type, chat_id = telepot.glance(msg)

        if content_type != 'text':
            self.sender.sendMessage('Invalid response')
            return

        if msg['text'][:4] == "loc ":
            latlngaltlist = msg['text'][4:].split(",")
            lat = 0.0
            lng = 0.0
            if len(latlngaltlist) > 3:
                self.sender.sendMessage("Too many bits given")
                return
            try:
                lat = float(latlngaltlist[0])
                lng = float(latlngaltlist[1])
                alt = int(latlngaltlist[2])
            except ValueError:
                self.sender.sendMessage("Incorrect types given")
                return
            self.qth = (lat,lng,alt)
            self.sender.sendMessage("Location stored")
            return
            
        elif msg['text'][:4] == "pre ":
            name = msg['text'][4:]
            if name in self.sats:
                tle = urllib2.urlopen(self.sats[name]).read()
                print(tle)
                tle = tle.split("<PRE>")[1][1:]
                tle = tle.split("</PRE>")[0]
                output_string = "Next 10 passes of {}\n=========\n".format(name)
                if not self.qth:
                    self.sender.sendMessage("Update your location before querying")
                    return
                p = predict.transits(tle, self.qth)
                for i in range(1,10):
                    transit = p.next()
                    output_string += "Start: %s | Duration (seconds): %f |  Peak Elevation (degrees): %f\n================\n" % (time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(float(transit.start))), transit.duration(), transit.peak()['elevation'])
                self.sender.sendMessage(output_string)
                return
            else:
                self.sender.sendMessage("Satellite not in our database")
                return
        
        elif msg['text'][:4] == "tle ":
            tle = msg['text'][4:]
            output_string = "Printing next 10 passes\n=========\n"
            if not self.qth:
                self.sender.sendMessage("Update your location before querying")
                return
            p = predict.transits(tle, self.qth)
            for i in range(1,10):
                transit = p.next()
                output_string += "Start: %s | Duration (seconds): %f |  Peak Elevation (degrees): %f\n================\n" % (time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(float(transit.start))), transit.duration(), transit.peak()['elevation'])
            self.sender.sendMessage(output_string)
            return


TOKEN = "606062906:AAFwRfSGNLwG2C36eyQ7W4Bk3El71Pi7D6M"

bot = telepot.DelegatorBot(TOKEN, [
    pave_event_space()(
        per_chat_id(), create_open, chatter, timeout=10000000000000000),
])
MessageLoop(bot).run_as_thread()
print('Listening ...')

while 1:
    time.sleep(10)
